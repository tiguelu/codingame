#!/usr/bin/env bash
# Dirty builder of concatenated sources into a single file
# This version will concatenate, in order of appearance, all the modules imported
# in the files: defs.py, code.py and main.py

SOURCE_PREFIXES='defs code main'
OUTFILE=merged_out.py

SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
if [[ -n "$1" ]]; then
  BUILD_DIR=$(realpath "$1")
else
  BUILD_DIR=${SCRIPT_DIR}
fi
OUTFILE="${BUILD_DIR}/"$(basename "${OUTFILE}")
PIPED_PREFIXES=$(echo "${SOURCE_PREFIXES}" | tr ' ' '|')
INTERNAL_IMPORT_REGEX='from .?('"${PIPED_PREFIXES}"').* import'

# Truncating output
: > "${OUTFILE}"

for section in ${SOURCE_PREFIXES}; do
  #for file in $(find "${BUILD_DIR}" -maxdepth 1 -name "${section}*" | sort); do
  grep -Po '(?<=from .)[^ ]+' "${BUILD_DIR}/${section}.py" | while IFS= read -r line; do
    file="${BUILD_DIR}/${line}.py"
    if [[ -e "${file}" ]]; then
      {
        echo "############"
        echo "# Content of $(basename "${file}")"
        echo "############"
        grep -Pv "${INTERNAL_IMPORT_REGEX}" "${file}"
        echo ""
        echo ""
      } >> "${OUTFILE}"
    fi
  done
done
