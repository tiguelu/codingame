import math


def frrad(s):
    f = float(s.replace(',', '.'))
    return math.radians(f)


lon = frrad(input())
lat = frrad(input())
n = int(input())
closest = (math.inf, '')
for i in range(n):
    defib = input().split(';')
    dlon = frrad(defib[4])
    dlat = frrad(defib[5])
    dname = defib[1]
    x = (dlon - lon) * math.cos((lat + dlat)/2)
    y = dlat - lat
    dist_name = ((math.sqrt(x**2 + y**2) * 6371), dname)
    closest = min(closest, dist_name)

print(closest[1])
