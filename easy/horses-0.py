import math

n = int(input())
minp = math.inf
allp = sorted(int(input()) for i in range(n))
for i in range(n):
    minp = min(minp, abs(allp[i] - allp[i-1]))

print(minp)
