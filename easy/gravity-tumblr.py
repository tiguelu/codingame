width, height = [int(i) for i in input().split()]
count = int(input())
raster = [input().count('#') for i in range(height)]
if count % 2:
    for w in range(width, 0, -1):
        print(''.join(['.' if w > raster[h] else '#' for h in range(height)]))
else:
    for h in range(height):
        print(''.join(['#' if w >= width - raster[h] else '.' for w in range(width)]))
