### ASCII Art
**[ascii.py](ascii.py)**  
String and array arithmetics  
<https://www.codingame.com/training/easy/ascii-art>
* * *

### Balanced Ternary Computer: Encode
**[balanced_ternary.py](balanced_ternary.py)**  
Represent an integer with balanced ternary guarisms  
<https://www.codingame.com/training/easy/balanced-ternary-computer-encode>
* * *

### Brackets, extreme edition
**[brackets.py](brackets.py)**  
Verify brackets are paired & nested  
<https://www.codingame.com/training/easy/brackets-extreme-edition>
* * *

### Chuck Norris
**[chuck_norris.py](chuck_norris.py)**  
Chuck Norris encoding in "unary" O only encoding  
<https://www.codingame.com/training/easy/chuck-norris>
* * *

### Defibrillators
**[defibrillators.py](defibrillators.py)**  
Find closest defibrillator  
<https://www.codingame.com/training/easy/defibrillators>
* * *

### Gravity Tumblr
**[gravity-tumblr.py](gravity-tumblr.py)**  
rotating landscape counterclockwise 90º letting # chars fall down  
<https://www.codingame.com/training/easy/gravity-tumbler>
* * *

### Horse Racing Duals
**[horse-racing.py](horse-racing.py)**  
Find the two closes horses in strength (sorting)  
<https://www.codingame.com/training/easy/horse-racing-duals>
* * *

### Mars Lander 1
**[mars-lander.py](mars-lander.py)**  
Conditions exercise. Safely land the spaceship in the flat area beneath it.  
<https://www.codingame.com/training/easy/mars-lander-episode-1>
* * *

### May the triforce be with you
**[triforce.py](triforce.py)**  
Print a triforce (3 ASCII triangles)  
<https://www.codingame.com/training/easy/may-the-triforce-be-with-you>
* * *

### MIME Type
**[mime.py](mime.py)**  
Determine the type of a file from the extension  
<https://www.codingame.com/training/easy/mime-type>
* * *

### Power of Thor Episode 1
**[thor.py](thor.py)**  
Thor finds his light.  
<https://www.codingame.com/training/easy/power-of-thor-episode-1>
* * *

### Temperatures
**[temperatures.py](temperatures.py)**  
Smallest temperature  
<https://www.codingame.com/training/easy/temperatures/solution>
* * *

### The Descent
**[descent.py](descent.py)**  
Destroy the mountains before the spaceship lands  
<https://www.codingame.com/training/easy/the-descent/solution>
* * *

### The River I
**[river-i.py](river-i.py)**  
Find the meeting point for two digital rivers  
<https://www.codingame.com/training/easy/the-river-i->
* * *

### The River II
**[river-ii.py](river-ii.py)**  
Find out if a given number is part of two or more digital rivers.  
<https://www.codingame.com/training/easy/the-river-ii->
* * *

