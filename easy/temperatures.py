import math

# Number of temperatures to analyse
n = int(input())
min_temp = math.inf
for temp in input().split():
    temp = int(temp)
    min_temp = temp if abs(temp) < abs(min_temp) else max(temp, min_temp) if abs(temp) == abs(min_temp) else min_temp

print(min_temp if n else 0)
