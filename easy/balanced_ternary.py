n = int(input())
carry = 0
translated = ''
pos = 0
glyph = {0: '0',
         1: '1',
         -1: 'T'
         }
while carry != n:
    r = round(n/(3**pos)) % 3
    if r == 2:
        r = -1
    translated = glyph[r] + translated
    carry += r * (3 ** pos)
    pos += 1

if carry == 0:
    translated = '0'

print(translated)
