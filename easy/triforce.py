n = int(input()) # lines of a triangle

print('.' + (2*n-2)*' ' + '*')
for i in range(1, n):
    print(' '*(2*n-i-1) + '**'*i + '*')
for i in range(n):
    print(' '*(n-i-1) + '**'*i + '* ' + '  '*(n-i-1) + '**'*i + '*')
