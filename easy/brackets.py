expression = input()
unmatched_brackets = []
opener = {')': '(', ']': '[', '}': '{'}
for c in expression:
    if c in '({[':
        unmatched_brackets.append(c)
    elif c in ')]}':
        if not unmatched_brackets or unmatched_brackets.pop() != opener[c]:
            print("false")
            exit()
print("false" if unmatched_brackets else "true")
