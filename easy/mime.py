n = int(input())  # Number of elements which make up the association table.
q = int(input())  # Number Q of file names to be analyzed.
mime = {}
for i in range(n):
    ext, mt = input().split()
    mime[ext.lower()] = mt
for i in range(q):
    fname = input()  # One file name per line.
    name, ext = fname.lower().rsplit('.', 1) if '.' in fname else ['', '']
    print(mime[ext] if ext in mime else 'UNKNOWN')
