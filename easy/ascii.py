l = int(input())
h = int(input())
t = input()
matrix = []
res = []
offset = ord('A')
n = ord('Z') - offset
for i in range(h):
    row = input()
    matrix.append(row)
    res.append('')

for c in t:
    i = ord(c.upper()) - offset
    if i < 0 or i > n:
        i = n + 1
    for j in range(h):
        res[j] += matrix[j][l*i:l*i+l]

for line in res:
    print(line)
