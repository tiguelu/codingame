light_x, light_y, thor_x, thor_y = [int(i) for i in input().split()]
while True:
    remaining_turns = int(input())  
    dir = ""
    if thor_y != light_y:
        if thor_y > light_y:
            thor_y -= 1
            dir = "N"
        else:
            thor_y += 1
            dir = "S"
            
    if thor_x != light_x:
        if thor_x > light_x:
            thor_x -= 1
            dir += "W"
        else:
            thor_x += 1
            dir += "E"
    print(dir)
