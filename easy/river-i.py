def next(point):
    res = point
    while point > 0:
        res += point % 10
        point //= 10
    return res

r1 = int(input())
r2 = int(input())
if r2 < r1:
    r1, r2 = r2, r1

while r1 != r2 and r2 <= 20000000:
    r1 = next(r1)
    while r1 > r2:
        r2 = next(r2)
print(r1)
