import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

message = input()
binmsg = ''
res = ''
# Write an action using print
# To debug: print("Debug messages...", file=sys.stderr)
for c in message:
    binmsg += f"{ord(c):07b}"

def implementation1():
    cod = binmsg.split('1')
    sp = ''
    ones = 0
    for s in cod:
        if s:
            # String of zeros
            if ones:
                print(f'{sp}0', '0' * ones, end='')
                sp = ' '
            print(f'{sp}00', '0'*len(s), end='')
            sp = ' '
            ones = 1
        else:
            # count string of 1
            ones += 1
    if sp:
        ones -= 1
    if ones:
        print(f'{sp}0', '0' * ones)
    else:
        print()


def implementation2():
    code = ['00', '0']
    encoding_digit = binmsg[0]
    sp = ''
    n = 0
    for c in binmsg:
        if c == encoding_digit:
            n += 1
        else:
            print(f'{sp}{code[int(encoding_digit)]}', '0'*n, end='')
            encoding_digit = c
            n = 1
            sp = ' '
    print(f'{sp}{code[int(c)]}', '0'*n)

implementation2()
