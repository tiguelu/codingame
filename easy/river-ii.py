import sys

def seq(n):
    res = n
    while n > 0:
        n, r = divmod(n, 10)
        res += r
    return res

n = int(input())
for i in range(n-1, 1, -1):
    if seq(i) == n:
        print("YES")
        sys.exit(0)
print("NO")

