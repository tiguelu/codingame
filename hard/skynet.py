# nn: the total number of nodes in the level, including the gateways
# nl: the number of links
# ne: the number of exit gateways
nn, nl, ne = [int(i) for i in input().split()]
links = [[int(j) for j in input().split()] for i in range(nl)]
exits = [int(input()) for i in range(ne)]  # a list of the exit nodes
g = {i: {} for i in range(nn)}
exiters = {}
for i in range(nn):
    g[i]['next'] = {link[0] if link[0] != i else link[1] for link in links if i in link}
    g[i]['exits'] = {e for e in exits if e in g[i]['next']}
    if g[i]['exits']:
        exiters[i] = len(g[i]['exits'])


def find_link_from(node):
    def sever_from(node):
        e = g[node]['exits'].pop()
        g[node]['next'].remove(e)
        g[e]['next'].remove(node)
        exiters[node] -= 1
        return sorted([e, node])

    if g[node]['exits']:
        return sever_from(node)
    finding_dead_paths = [node]
    visited = set()
    while finding_dead_paths:
        node = finding_dead_paths.pop()
        visited.add(node)
        for neighbour in g[node]['next']:
            if neighbour in visited:
                continue
            if g[neighbour]['exits']:
                if len(g[neighbour]['exits']) > 1:
                    return sever_from(neighbour)
                finding_dead_paths.append(neighbour)
    exits, exiter = max((exiters[e], e) for e in exiters)
    return sever_from(exiter)


while True:
    si = int(input())  # node Skynet agent is positioned on
    link = find_link_from(si)
    print(link[0], link[1])  # Printing node we want to sever
