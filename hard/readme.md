### Skynet revolution - Episode 2
**[skynet.py](skynet.py)**  
Graphs, Breadth First Search, Dijkstra, tree traversal  
Finds the best link to cut to prevent access to an exit  
<https://www.codingame.com/training/hard/skynet-revolution-episode-2>

