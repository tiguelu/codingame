###
# This solution does NOT pass the 2 tests with dead paths
###

# nodes: the total number of nodes in the level, including the gateways
# links: the number of links
# exits: the number of exit gateways
nn, nl, ne = [int(i) for i in input().split()]

links = [[int(j) for j in input().split()] for i in range(nl)]
# the index of a gateway node
exits = [int(input()) for i in range(ne)]


def exit_link_from(node):
    nodes_exits = {}
    next_level_search = [[node]]
    while next_level_search:
        nodes = next_level_search.pop(0)
        next_level = []
        max_exits = max_node = 0
        for node in nodes:
            nodes_exits[node] = [0, []]
            for link in links:
                if node in link:
                    neighbour = link[1] if node == link[0] else link[0]
                    if neighbour in exits:
                        nodes_exits[node][0] += 1
                        nodes_exits[node][1].append(link)
                        if nodes_exits[node][0] > max_exits:
                            max_exits = nodes_exits[node][0]
                            max_node = node
                    elif neighbour not in nodes_exits:
                        next_level.append(neighbour)
        if max_exits:
            return nodes_exits[max_node][1][0]
        next_level_search.append(next_level)


while True:
    si = int(input())  # node Skynet agent is positioned on
    # Printing node we want to sever
    link = exit_link_from(si)
    links.remove(link)
    print(link[0], link[1])
