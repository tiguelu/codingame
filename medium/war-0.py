import sys


def card_val(c):
    c = c[:-1]
    if c.isnumeric():
        return int(c)
    return (
        11 if c == 'J' else
        12 if c == 'Q' else
        13 if c == 'K' else
        14
    )


cards = {}
played_cards = {1: [], 2: []}
n = int(input())
cards[1] = [card_val(input()) for i in range(n)]
n = int(input())
cards[2] = [card_val(input()) for i in range(n)]


def do_war():
    for i in (1, 2):
        if len(cards[i]) < 4:
            print("PAT")
            sys.exit(0)
        played_cards[i] += cards[i][:4]
        cards[i] = cards[i][4:]


def play_hand():
    global played_cards
    if played_cards[1][-1] == played_cards[2][-1]:
        do_war()
        play_hand()
    elif played_cards[1][-1] > played_cards[2][-1]:
        cards[1] += played_cards[1] + played_cards[2]
    else:
        cards[2] += played_cards[1] + played_cards[2]
    played_cards = {1: [], 2: []}


rounds = 0
while True:
    for i in (1, 2):
        if len(cards[i]) == 0:
            winner = 1 if i == 2 else 2
            print(winner, rounds)
            sys.exit(0)
        played_cards[i].append(cards[i].pop(0))
    play_hand()
    rounds += 1

