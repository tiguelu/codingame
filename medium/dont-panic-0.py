nb_floors, width, nb_rounds, exit_floor, exit_pos, nb_total_clones, nb_additional_elevators, nb_elevators = [int(i) for i in input().split()]
elevators = {}
for i in range(nb_elevators):
    floor, pos = [int(j) for j in input().split()]
    elevators[floor] = pos
while True:
    clone_floor, clone_pos, direction = input().split()
    if direction == 'NONE':
        print('WAIT')
        continue
    clone_floor = int(clone_floor)
    clone_pos = int(clone_pos)
    goto_pos = exit_pos if clone_floor == exit_floor else elevators[clone_floor]
    if ((goto_pos > clone_pos and direction == 'LEFT')
            or (goto_pos < clone_pos and direction == 'RIGHT')):
        print("BLOCK")
    else:
        print("WAIT")
