import math

MAX_V_SPEED = 40
MAX_H_SPEED = 20
ANGLE_MAX_CHANGE_X = -90
ANGLE_FAST_CHANGE_X = -45
ANGLE_NORMAL_CHANGE_X = -30
ANGLE_SUSTAIN_V_SPEED = -20
ANGLE_MIN_CHANGE_X = -10
X = 'x'
Y = 'y'


def coords(str_tuple):
    return {X: int(str_tuple[0]), Y: int(str_tuple[1])}


# Number of points used to draw the surface:
surface_points = int(input())
# tuples of (X, Y) coordinates to extrapolate the surface:
points = [coords(input().split()) for i in range(surface_points)]
highest_peak = coords(('0', '0'))
for i in range(1, surface_points):
    if (points[i][Y] == points[i-1][Y]
            and points[i][X] - points[i-1][X] >= 1000):
        flat_start = points[i-1][X]
        flat_end = points[i][X]
        landing_height = points[i][Y]
        flat_length = flat_end - flat_start
        flat_center = (flat_end + flat_start) // 2


def approx_dist_surface(x, y, direction):
    # Get min vertical distance in next 2000m
    dist = math.inf
    for p in points:
        if direction == 1:
            if p[X] < x:
                continue
        else:
            if p[X] > x:
                continue
        dist = min(dist, abs(y-p[Y]))
        if abs(p[X] - x) > 2000:
            break
    return dist


def new_tilt(x, y, h_speed, v_speed, rotate, power):
    distance_to_flat = abs(x - flat_center)
    if h_speed == 0:  # Not moving horizontally
        direction = 0
        if distance_to_flat > flat_length / 2:
            # We are not over the flat area, must be fixed
            desired_direction = 1 if x < flat_center else -1
        else:
            # We are over the flat area
            desired_direction = 0
    else:  # Moving horizontally
        direction = -1 if h_speed < 0 else 1
        if distance_to_flat <= flat_length / 2:
            # We are over the flat area, speed must be compensated
            desired_direction = -direction
        elif abs(h_speed) > MAX_H_SPEED:
            # Moving quite fast, might need compensation if near the flat
            if distance_to_flat < flat_length:
                desired_direction = -direction
            else:
                # Still far from flat
                if ((x < flat_center and direction == -1)
                        or (x > flat_center and direction == 1)):
                    # Moving on the wrong direction, speed must be compensated
                    desired_direction = -direction
                elif abs(h_speed) < MAX_H_SPEED * 2:
                    # Moving on right direction not too fast, can accelerate
                    desired_direction = direction
                elif abs(h_speed) < MAX_H_SPEED * 2 + 3:
                    # Proper speed, no change needed
                    desired_direction = 0
                else:
                    # Moving on right direction too fast, must be compensated
                    desired_direction = -direction
        else:
            # Moving too slow, must accelerate
            desired_direction = 1 if x < flat_center else -1

    ad = approx_dist_surface(x, y, direction)
    if distance_to_flat <= flat_length / 2 and abs(h_speed) <= MAX_H_SPEED:
        if y > landing_height + 50:
            target_tilt = desired_direction * ANGLE_SUSTAIN_V_SPEED
        else:
            target_tilt = 0
        if v_speed > -MAX_V_SPEED / 2:
            target_thrust = 2
        elif v_speed > -MAX_V_SPEED + 1:
            target_thrust = 3
        else:
            target_thrust = 4
    elif ad > 200:
        if v_speed > -MAX_V_SPEED / 4:
            target_tilt = desired_direction * ANGLE_MAX_CHANGE_X
            if ((desired_direction > 0 and rotate > 0)
                    or (desired_direction < 0 and rotate < 0)):
                # Actual angle opposes to our desired direction
                # We can save fuel by not thrusting yet
                target_thrust = 0
            else:
                target_thrust = 4
        else:
            target_tilt = desired_direction * ANGLE_SUSTAIN_V_SPEED
            target_thrust = 4
    elif ad > 50:
        if v_speed < -MAX_V_SPEED / 4:
            target_tilt = desired_direction * ANGLE_MIN_CHANGE_X
            target_thrust = 4
        elif -MAX_V_SPEED / 4 <= v_speed < 0:
            target_tilt = desired_direction * ANGLE_SUSTAIN_V_SPEED
            target_thrust = 4
        else:
            target_tilt = desired_direction * ANGLE_NORMAL_CHANGE_X
            target_thrust = 4
    else:
        target_tilt = 0
        target_thrust = 4

    return target_tilt, target_thrust


while True:
    # h_speed: the horizontal speed (in m/s), can be negative.
    # v_speed: the vertical speed (in m/s), can be negative.
    # fuel: the quantity of remaining fuel in liters.
    # rotate: the rotation angle in degrees (-90 to 90).
    # power: the thrust power (0 to 4).
    x, y, h_speed, v_speed, fuel, rotate, power = [int(i) for i in input().split()]

    tilt, thrust = new_tilt(x, y, h_speed, v_speed, rotate, power)

    # rotate power. rotate is the desired rotation angle. power is the desired
    # thrust power.
    print(tilt, thrust)
