n = int(input())
v = [int(i) for i in input().split()]

peak = 0
low = 0
lower = {}
for i in range(1, n):
    if v[i] > v[peak]:
        if low == peak:
            peak = low = i
        else:
            for j in lower:
                if lower[j] > v[low]:
                    lower[j] = v[low]
            lower[v[peak]] = v[low]
            peak = low = i
    elif v[i] < v[peak]:
        if v[i] < v[low]:
            lower[v[peak]] = v[i]
            low = i

min_diff = 0
for i in lower:
    min_diff = min(min_diff, lower[i]-i)

print(min_diff)
