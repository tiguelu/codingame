w, h = [int(i) for i in input().split()]
n = int(input())
x0, y0 = [int(i) for i in input().split()]

ix = iy = 0
fx, fy = w, h
while True:
    bomb_dir = input()
    if 'U' in bomb_dir:
        fy = y0
    elif 'D' in bomb_dir:
        iy = y0
    if 'R' in bomb_dir:
        ix = x0
    elif 'L' in bomb_dir:
        fx = x0
    x0 = (ix + fx) // 2
    y0 = (iy + fy) // 2
    print(x0, y0)
