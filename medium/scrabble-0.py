scores = {
  'e': 1, 'a': 1, 'i': 1, 'o': 1, 'n': 1, 'r': 1, 't': 1, 'l': 1, 's': 1, 'u': 1,
  'd': 2, 'g': 2,
  'b': 3, 'c': 3, 'm': 3, 'p': 3,
  'f': 4, 'h': 4, 'v': 4, 'w': 4, 'y': 4,
  'k': 5,
  'j': 8, 'x': 8,
  'q': 10, 'z': 10
}

n = int(input())
dictionary = [input().lower() for i in range(n)]
letters = list(input().lower())
given = len(letters)

max_score = 0
best_word = ''
for w in dictionary:
    if len(w) > given:
        continue
    lttrs = letters.copy()
    score = 0
    for c in w:
        if c in lttrs:
            lttrs.remove(c)
            score += scores[c]
        else:
            break
    if len(w) == given-len(lttrs) and score > max_score:
        max_score = score
        best_word = w

print(f"{best_word}")

