import bisect


def not_in_points(x):
    i = bisect.bisect_left(points, x)
    return i == n or points[i] != x


n = int(input())
points = sorted([[int(j) for j in input().split()] for i in range(n)])
found = set()
for i in range(n):
    p1 = points[i]
    for j in range(i, n):
        p2 = points[j]
        if p1 == p2:
            continue
        DeltaX = p2[0] - p1[0]
        DeltaY = p2[1] - p1[1]
        p3 = [p2[0]-DeltaY, p2[1]+DeltaX]
        if not_in_points(p3):
            continue
        p4 = [p3[0]-DeltaX, p3[1]-DeltaY]
        if not_in_points(p4):
            continue
        found.add(tuple(sorted((tuple(p1), tuple(p2), tuple(p3), tuple(p4)))))
print(len(found))

