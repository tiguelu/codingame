### Aneo
**[aneo.py](aneo.py)**  
Mathematics, Arithmetics, Intervals, Loops  
Problem of traffic lights changing color at intervals and finding max speed for all green. Floating point rounding errors.  
<https://www.codingame.com/training/medium/aneo>
* * *

### Continuing squares on pegs
**[squares-pegs.py](squares-pegs.py)**  
Geometry, Vectorial calculus  
Find all possible squares in a grid of points  
<https://www.codingame.com/training/medium/counting-squares-on-pegs>
* * *

### Don't panic - Episode 1
**[dont-panic.py](dont-panic.py)**  
Conditions  
Help one of the robot clones exit the building grid  
<https://www.codingame.com/training/medium/don't-panic-episode-1>
* * *

### Mars Lander Episode 2
**[mars-lander.py](mars-lander.py)**  
Distances, Trigonometry, Genetic algorithms  
Landing the spaceship in the flat land.  
<https://www.codingame.com/training/medium/mars-lander-episode-2>
* * *

### Scrabble
**[scrabble.py](scrabble.py)**  
Conditions, loops, associative arrays, hash tables  
Finding the word that scores the most from a list  
<https://www.codingame.com/training/medium/scrabble>
* * *

### Shadows of the night
**[shadows-night.py](shadows-night.py)**  
Binary search, intervals  
Batman jumping to windows with binary search algorithm, finding the bomb.  
<https://www.codingame.com/training/medium/shadows-of-the-knight-episode-1>
* * *

### Skynet revolution - Episode 1
**[skynet.py](skynet.py)**  
Graphs, Breadth First Search  
Finds the closest link to a given node, leading to an exit  
<https://www.codingame.com/training/medium/skynet-revolution-episode-1>

### Stock Exchange Losses
**[stock.py](stock.py)**  
Arrays, loops, conditions  
Find the biggest loss in a sequence of stock prices  
<https://www.codingame.com/training/medium/stock-exchange-losses>

### There is no spoon - Episode 1
**[no-spoon.py](no-spoon.py)**  
Lists, loops, multidimensional arrays  
Print the node that contains a power cell and its neighbours  
<https://www.codingame.com/training/medium/there-is-no-spoon-episode-1>
* * *

### War
**[war.py](war.py)**  
Queues  
Find who is the winner of a given card distribution in the War game  
<https://www.codingame.com/training/medium/winamax-battle>
* * *
