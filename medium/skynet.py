# nodes: the total number of nodes in the level, including the gateways
# links: the number of links
# exits: the number of exit gateways
nn, nl, ne = [int(i) for i in input().split()]

links = [[int(j) for j in input().split()] for i in range(nl)]
# the index of a gateway node
exits = [int(input()) for i in range(ne)]


def exit_link_from(node):
    visited_in_search = []

    def find_exit_in_links(node):
        next_level_search = []
        visited_in_search.append(node)
        for link in links:
            if node in link:
                for e in exits:
                    if e in link:
                        return link
                neighbour = link[1] if node == link[0] else link[0]
                if neighbour not in visited_in_search:
                    next_level_search.append(neighbour)
        for node in next_level_search:
            link = find_exit_in_links(node)
            if link:
                return link
        else:
            return None

    return find_exit_in_links(node)


while True:
    si = int(input())  # node Skynet agent is positioned on
    # Printing node we want to sever
    link = exit_link_from(si)
    links.remove(link)
    print(link[0], link[1])
