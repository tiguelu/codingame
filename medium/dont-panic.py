nb_floors, width, nb_rounds, exit_floor, exit_pos, nb_total_clones, nb_additional_elevators, nb_elevators = [int(i) for i in input().split()]
elevators = {exit_floor: exit_pos}
for i in range(nb_elevators):
    floor, pos = [int(j) for j in input().split()]
    elevators[floor] = pos
while True:
    clone_floor, clone_pos, direction = input().split()
    if direction == 'NONE':
        print('WAIT')
        continue
    clone_floor = int(clone_floor)
    clone_pos = int(clone_pos)
    if ((direction == 'LEFT' and clone_pos < elevators[clone_floor])
            or (direction == 'RIGHT') and clone_pos > elevators[clone_floor]):
        print("BLOCK")
    else:
        print("WAIT")
