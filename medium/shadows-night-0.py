w, h = [int(i) for i in input().split()]
n = int(input())
x0, y0 = [int(i) for i in input().split()]

ix = 0
iy = 0
fx = w - 1
fy = h - 1
while True:
    bomb_dir = input()
    if 'U' in bomb_dir:
        if fy == iy + 1:
            y0 = iy
        else:
            fy = y0
            y0 = iy + int((fy - iy) / 2)
    elif 'D' in bomb_dir:
        if iy == fy - 1:
            y0 = fy
        else:
            iy = y0
            y0 = iy + int((fy - iy) / 2)
    if 'R' in bomb_dir:
        if ix == fx - 1:
            x0 = fx
        else:
            ix = x0
            x0 = ix + int((fx - ix) / 2)
    elif 'L' in bomb_dir:
        if fx == ix + 1:
            x0 = ix
        else:
            fx = x0
            x0 = ix + int((fx - ix) / 2)
    print(f"{x0} {y0}")
