width = int(input())  
height = int(input())
grid = [input() for y in range(height)]
for y in range(height):
    for x in range(width):
        if grid[y][x] == '0':
            out = f"{x} {y}"
            for nx in range(x+1, width):
                if grid[y][nx] == "0":
                    out += f" {nx} {y}"
                    break
            else:
                out += " -1 -1"
            for ny in range(y+1, height):
                if grid[ny][x] == '0':
                    out += f" {x} {ny}"
                    break
            else:
                out += " -1 -1"
            print(out)

