from fractions import Fraction


def ints(str_tuple):
    return int(str_tuple[0]), int(str_tuple[1])


max_speed = int(input())
light_count = int(input())
lights = [ints(input().split()) for i in range(light_count)]

for speed in range(max_speed, 0, -1):
    speed_ms = Fraction(speed * 1000, 3600)
    for distance, duration in lights:
        time = Fraction(distance, speed_ms)
        light_is_green = (Fraction(time, duration) % 2) < 1
        if not light_is_green:
            break
    if light_is_green:
        print(speed)
        exit(0)
