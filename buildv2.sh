#!/usr/bin/env bash
# Dirty builder of concatenated sources into a single file
# This version will concatenate, in order of appearance, all the modules imported
# in INCLUDE_MODULE
# If LOOP_MODULE exists and it is not already included, it will be included too

TMP_BUILD_FILE=merged_out.py.build
OUTFILE=merged_out.py

SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
if [[ -n "$1" ]]; then
  BUILD_DIR=$(realpath "$1")
else
  BUILD_DIR=${SCRIPT_DIR}
fi
TMP_BUILD_FILE="${BUILD_DIR}/"$(basename "${TMP_BUILD_FILE}")
OUTFILE="${BUILD_DIR}/"$(basename "${OUTFILE}")
INCLUDE_MODULE="include"
LOOP_MODULE="loop"
LOOP_FILE="${BUILD_DIR}/${LOOP_MODULE}.py"
LOOP_IMPORT='^from \.'"${LOOP_MODULE}"' import \*$'
INCLUDE_MODULES='(?<=^from \.)[^ ]+'
INTERNAL_IMPORT='^from \.[^ ]+ import \*'


output_file () {
  module=$(echo "$1" | sed 's#\.#/#g')
  file="${BUILD_DIR}/${module}.py"
  if [[ -e "${file}" ]]; then
    {
      echo "############"
      echo "# Content of ${module}.py"
      echo "############"
      grep -Pv "${INTERNAL_IMPORT}" "${file}"
      echo ""
      echo ""
    } >> "${TMP_BUILD_FILE}"
  fi
}

# Truncating output
: > "${TMP_BUILD_FILE}"

grep -Po "${INCLUDE_MODULES}" "${BUILD_DIR}/${INCLUDE_MODULE}.py" | while IFS= read -r line; do
  output_file "$line"
done

if [[ -e "$LOOP_FILE" ]]; then
  grep -P "${LOOP_IMPORT}" "${BUILD_DIR}/${INCLUDE_MODULE}.py"
  if [[ $? -eq 1 ]]; then
    output_file "${LOOP_MODULE}"
  fi
fi

mv "$TMP_BUILD_FILE" "$OUTFILE"