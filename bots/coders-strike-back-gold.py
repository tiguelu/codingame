import sys
import math

x = 'x'
y = 'y'
vx = 'vx'
vy = 'vy'
angle = 'angle'
cp = 'cp'
keys = (x, y, vx, vy, angle, cp)
boost = 1

laps = int(input())
cp_count = int(input())
CP = [dict(zip((x, y), [int(j) for j in input().split()])) for i in range(cp_count)]
prev_cp_op0 = visits_op0 = 0
prev_cp_op1 = visits_op1 = 0


def dbg(*s):
    print(*s, file=sys.stderr)


def distance(p1, p2):
    return math.sqrt((p1[x]-p2[x])**2 + (p1[y]-p2[y])**2)


def angdif(a, b):
    b = 360 - b
    dif = abs(b - a)
    return dif if dif < 180 else 360 - dif


def collision(p1, p2):
    n1 = {x: p1[x] + p1[vx], y: p1[y] + p1[vy]}
    n2 = {x: p2[x] + p2[vx], y: p2[y] + p2[vy]}
    return distance(n1, n2) <= 800


def approx_landing(cp, p, max_turns=10):
    # Turns for landing if we use thrust = 0
    d = distance(cp, p)
    turns = 0
    new_coord = {
        x: p[x],
        y: p[y]
    }
    nvx, nvy = p[vx], p[vy]
    while d > 600:
        new_coord[x] += nvx
        new_coord[y] += nvy
        d = distance(cp, new_coord)
        if turns > max_turns:
            return None
        turns += 1
        nvx = int(nvx * 0.85)
        nvy = int(nvy * 0.85)
        if nvx == 0 and nvy == 0:
            return None
    return turns


def get_deviation(cp, p):
    # distance vector from pod to check point
    d = {x: cp[x] - p[x],
         y: cp[y] - p[y]}
    mod_d = math.hypot(d[x], d[y])
    alpha = round(math.degrees(math.acos(d[x] / mod_d)))
    if d[y] > 0:
        # This is for consistency with provided 'angle' that € [0, 2pi) clockwise
        alpha = 360 - alpha
    deviation = angdif(alpha, p[angle])

    return deviation


def next_out(cp, p):
    tgt_coord = {x: cp[x] - p[vx], y: cp[y] - p[vy]}
    tgt_thrust = math.hypot(tgt_coord[x] + p[x], tgt_coord[y] + p[y])
    dbg(f"tgt_thrust:{tgt_thrust}")
    tgt_thrust = min(100, round(tgt_thrust))

    return tgt_coord, tgt_thrust


def speed(p):
    return math.hypot(p[vx], p[vy])


def init():
    global pods, opon, prev_cp_op0, visits_op0, prev_cp_op1, visits_op1
    
    pods = [dict(zip(keys, [int(j) for j in input().split()])) for i in range(2)]
    dbg(pods)
    opon = [dict(zip(keys, [int(j) for j in input().split()])) for i in range(2)]
    if opon[0][cp] != prev_cp_op0:
        prev_cp_op0 = opon[0][cp]
        visits_op0 += 1
    if opon[1][cp] != prev_cp_op1:
        prev_cp_op1 = opon[1][cp]
        visits_op1 += 1


def pod_runner(i):
    global boost

    r = pods[i]
    ncp = CP[r[cp]]
    deviation = get_deviation(ncp, r)
    max_land = 4
    turns2cp = approx_landing(ncp, r, 6)
    dbg(f"turns2cp:{turns2cp} ncp:{r[cp]}")
    if deviation > 90:
        coord = {x: ncp[x], y: ncp[y]}
        thrust = 0
    elif turns2cp and turns2cp <= max_land:
        turnto = (r[cp] + 1) % cp_count
        coord, thrust = next_out(CP[turnto], r)
        deviation = get_deviation(CP[turnto], r)
        if deviation > 90:
            thrust = 0
        else:
            thrust = round(math.cos(math.radians(deviation))*thrust)
        dbg(f"Turning to new check point with thrust {thrust}")
    else:
        coord, thrust = next_out(ncp, r)
        if boost and distance(r, ncp) > 5000 and deviation < 15:
            thrust = 'BOOST'
            boost = 0
        elif deviation > 20:
            thrust = round(math.cos(math.radians(deviation))*thrust)
            dbg(f"Big angle, reducing thrust by 10% {thrust}")
    if collision(r, opon[0]) or collision(r, opon[1]):
        thrust = 'SHIELD'

    return coord, thrust


def pod1_runner():
    global coord1, thrust1
    coord1, thrust1 = pod_runner(0)


def pod2_runner():
    global coord2, thrust2
    coord2, thrust2 = pod_runner(0)


def pod2_follower():
    global coord2, thrust2

    thrust2 = 100
    coord2 = {x: opon[0][x], y: opon[0][y]}
    if distance(pods[0], pods[1]) > 800:
        if collision(pods[1], opon[0]) or collision(pods[1], opon[1]):
            thrust2 = 'SHIELD'


def pod2_1000():
    global coord2, thrust2

    c = CP[0]
    p = pods[1]
    d = {x: c[x] - p[x],
         y: c[y] - p[y]}
    dist = math.hypot(d[x], d[y])
    deviation = get_deviation(c, p)
    tgt = [o for o in opon if o[cp] == 0]
    if len(tgt) == 2:
        if visits_op0 > visits_op1:
            tgt = opon[0]
        elif visits_op1 > visits_op0:
            tgt = opon[1]
        elif distance(c, opon[0]) < distance(c, opon[1]):
            tgt = opon[0]
        else:
            tgt = opon[1]
    elif len(tgt) == 1:
        tgt = tgt.pop()

    dbg(f"dist:{dist} v:{speed(p)} dev:{deviation}")
    if tgt and (pods[0][cp] != 0 or (pods[0][cp] == 0 and distance(pods[0], c) > distance(tgt, c))):
        coord2 = {x: c[x], y: c[y]}
        if dist > 2000:
            thrust2 = 100
        elif dist > 300:
            thrust2 = 50 if speed(p) < 45 else 0
        # if collision(p, tgt):
        #     #coord2 = {x: tgt[x] + tgt[vy], y: tgt[y] + tgt[vy]}
        #     thrust2 = 'SHIELD'
    elif dist < 1100:
        coord2 = {x: p[x] - d[x],
                  y: p[y] - d[y]}
        if deviation == 180 and speed(p) < 20:
            thrust2 = 20
        else:
            thrust2 = 0
    else:
        coord2 = {x: c[x],
                  y: c[y]}
        if dist > 1500 and deviation < 90:
            thrust2 = round(100 * math.cos(math.radians(deviation)))
        elif deviation != 0:
            thrust2 = 0
        elif dist > 4000:
            thrust2 = 100
        elif dist > 1300 and speed(p) < 35:
            thrust2 = 50
    if collision(p, opon[0]) or collision(p, opon[1]):
        thrust2 = 'SHIELD'


def pod2_last():
    global coord2, thrust2
    last = CP[cp_count - 1]
    penul = CP[cp_count - 2]
    d = {x: last[x] - penul[x],
         y: last[y] - penul[y]}
    center_coord = {x: penul[x] + round(d[x]/2),
              y: penul[y] + round(d[y]/2)}
    perp_ang = math.atan2(center_coord[y], center_coord[x]) + (math.pi / 2)
    perp_vector = {x: round(1000 * math.cos(perp_ang)),
                   y: round(1000 * math.sin(perp_ang))}
    perp_coord = {x: center_coord[x] + perp_vector[x],
                  y: center_coord[y] + perp_vector[y]}
    coord2 = perp_coord
    turns2center = approx_landing(perp_coord, pods[1])
    if turns2center is None:
        thrust2 = 50
    elif turns2center < 6:
        thrust2 = 0
    else:
        thrust2 = 20
    if distance(pods[0], pods[1]) > 800:
        if collision(pods[1], opon[0]) or collision(pods[1], opon[1]):
            thrust2 = 'SHIELD'


while True:
    init()
    pod1_runner()
    pod2_1000()
    print(f"{coord1[x]} {coord1[y]} {thrust1}")
    print(f"{coord2[x]} {coord2[y]} {thrust2}")

