import sys
import math

def dbg(*v):
    print(*v, file=sys.stderr)

def init():
    global x, y, next_checkpoint_x, next_checkpoint_y, next_checkpoint_dist, next_checkpoint_angle
    global opponent_x, opponent_y
    global turn, prev_check_x, prev_x, prev_y, tgt_x, tgt_y, thrust
    
    x, y, next_checkpoint_x, next_checkpoint_y, next_checkpoint_dist, next_checkpoint_angle = [int(i) for i in input().split()]
    opponent_x, opponent_y = [int(i) for i in input().split()]
    
    if turn == -1:
        prev_check_x = next_checkpoint_x
        prev_x = x
        prev_y = y
        turn = 0
    elif turn > 0:
        turn -= 1
    
    v = (x - prev_x, y - prev_y)
    v = [round(0.847*c) for c in v]  # Next v considers .153 friction
    Tideal = (next_checkpoint_x - x, next_checkpoint_y -y)
    T = [Tideal[i]-v[i] for i in range(2)]
    tgt_x = T[0] + x
    tgt_y = T[1] + y
    thrust = math.sqrt(T[0]**2 + T[1]**2)
    dbg(f"v {v}  Tideal {Tideal}  T {T}  mod {thrust}")
    thrust = round(thrust) if thrust < 100 else 100
    
    
def refine():
    global next_checkpoint_angle, next_checkpoint_dist, next_checkpoint_x, prev_check_x
    global turn, thrust, boost
    global opponent_x, opponent_y, x, y

    if abs(next_checkpoint_angle) > 90:
        thrust = 0
    elif prev_check_x != next_checkpoint_x:
        thrust = round(thrust*math.cos(math.radians(next_checkpoint_angle)))
        dbg(f"new check point with {thrust}")
        turn = 3
    elif next_checkpoint_dist < 2000 and next_checkpoint_angle > 20:
        thrust = 10
        print("derrapeeeee", file=sys.stderr)
    elif turn:
        thrust = round(thrust*math.cos(math.radians(next_checkpoint_angle)))
        dbg(f"Turning {turn} with thrust {thrust}")
    else:
        if boost and next_checkpoint_dist > 5000 and abs(next_checkpoint_angle) < 10:
            thrust = 'BOOST'
            boost = 0
        elif abs(next_checkpoint_angle) > 20:
            thrust = round(.9*thrust)
            dbg(f"Big angle, reducing thrust by 10% {thrust}")
        
    
turn = -1
boost = 1
while True:
    init()
    dbg(f"next_dist {next_checkpoint_dist}  turn {turn}  angle {next_checkpoint_angle}")
    refine()
    
    print(tgt_x, tgt_y, thrust)
    prev_check_x = next_checkpoint_x
    prev_x = x
    prev_y = y
    
