from .instantiation import *


def radar_area_at(position):
    area = 0
    for _ in stage.choose_closer(*position, RADAR_RANGE):
        area += 1
    return area


"""
Position generators for radars in the map. Can be used to iterate 
through them or to generate a list
"""


def generator_oscardist():
    # for x in range(5, WIDTH, 9):
    for x in range(1, WIDTH, 8):
        # for y in range(4, HEIGHT, 9):
        for y in range(4, HEIGHT, 10):
            yield x, y
    # for x in range(1, WIDTH, 9):
    for x in range(5, WIDTH, 8):
        # for y in range(-1, HEIGHT, 9):
        for y in range(0, HEIGHT, 9):
            if x > 0:
                yield x, y


OSCAR_LOCS = list(generator_oscardist())


"""
Sorters take a list of positions or a generator and returns them sorted
by different criteria
"""

# Base positions for sorting functions
RADAR_LOCS = OSCAR_LOCS


def sort_by_coverage_dist(robot):
    def score(radar_pos):
        dist2robot = robot.dist(*radar_pos)
        radar_coverage = radar_area_at(radar_pos)
        return radar_coverage, dist2robot
    return sorted(RADAR_LOCS, key=score, reverse=True)


def sort_by_closer(robot):
    return sorted(RADAR_LOCS, key=lambda p: robot.dist(*p))


def sort_by_coverage_closer(robot):
    def score(radar_pos):
        dist2robot = robot.dist(*radar_pos)
        radar_coverage = radar_area_at(radar_pos)
        return radar_coverage, -dist2robot
    return sorted(RADAR_LOCS, key=score, reverse=True)


def sort_by_coverage_closer2center():
    def score(radar_pos):
        center = (14, 7)
        dist2center = math.sqrt((center[0] - radar_pos[0]) ** 2 + (center[1] - radar_pos[1]) ** 2)
        radar_coverage = radar_area_at(radar_pos)
        return radar_coverage, 1/dist2center
    return sorted(RADAR_LOCS, key=score, reverse=True)


# Base positions sorted by coverage and max distance to a robot in (0, 0)
MAX_COVERAGE_AND_MAX_DIST_TO_00 = [
    (25, 4), (21, 9), (17, 4), (9, 4), (13, 9), (5, 9), (1, 4), (29, 9), (21, 0),
    (25, 14), (13, 0), (5, 0), (17, 14), (9, 14), (1, 14), (29, 0)
]

ENEMY_RADAR_LOCS = [
    (5, 3), (5, 11), (15, 3), (15, 11), (25, 4), (25, 11), (10, 14), (20, 14)
]


"""
next_radar functions return the position to place the next radar based on
different criteria
"""


def next_radar(*args, src=RADAR_LOCS, fn=None):
    if callable(fn):
        src = fn(*args)
    for point in src:
        if not stage[point].radar and point not in potential_radars.values():
            return point
    else:
        return None


def next_radar_max_coverage_max_dist(robot):
    # Radar points with bigger area and further away, are provided first
    return next_radar(robot, fn=sort_by_coverage_dist)


def next_radar_max_coverage_min_dist(robot):
    # Radar points with bigger area and closer, are provided first
    return next_radar(robot, fn=sort_by_coverage_closer)


def next_radar_min_dist(robot):
    return next_radar(robot, fn=sort_by_closer)


def next_radar_max_coverage_max_dist_00(robot):
    # Radar points with bigger area and further away from (0, 0) are provided first
    return next_radar(src=MAX_COVERAGE_AND_MAX_DIST_TO_00)


def next_radar_max_coverage_closer2center(robot):
    # Radar points with bigger area and closer to the center, are provided first
    return next_radar(robot, fn=sort_by_coverage_closer2center)
