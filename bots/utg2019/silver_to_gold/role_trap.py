from .next_radar import *
from .role_ore import *


def trap(bot):
    if bot.item == NONE:
        if bot.x == 0:
            return bot.request("TRAP", "I am Íñigo Montoya")
        else:
            return bot.reset_role(ore)
    elif bot.item == TRAP:
        for cell in safe_ore_cells[bot:].eval('ore >= 2'):
            if (cell.x, cell.y) in RADAR_LOCS:
                continue
            potential_traps[bot.id] = cell
            return bot.dig(cell.x, cell.y, f"Morirás con {cell.ore}")

        # At this point we either have no visibility of places with ore, or
        # all ore cells have been dug or have less than 2 ores.
        # We'll try to put it in some non-holed place near the robot.
        for cell in stage[bot:].eval("hole == 0 and x > 2"):
            return bot.dig(cell.x, cell.y, 'Viva el queso')
    else:
        return bot.reset_role(ore)
