from .instantiation import *
from .role_noores import *


def ore(bot):
    if bot.item == ORE:
        return bot.move(0, bot.y, 'Morriña')

    cell = bot.role_data.get('cell', None)
    if cell is not None:
        return bot.dig(cell.x, cell.y, 'Orr Orr a mandar')

    return bot.reset_role(noores)
