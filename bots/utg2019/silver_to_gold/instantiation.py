from .definitions.entities import *
from .definitions.stage import *

entities = MockyDictIter('id', 'type', 'x', 'y', 'item', new_from=Entity, keep=1)
stage = Mocky2DIter('ore', 'hole', width=WIDTH, height=HEIGHT, new_from=Cell, keep=1)
ore_cells = stage.eval("ore and ore>0")
safe_ore_cells = ore_cells.eval("not unsafe and not trap")


def read_turn_data():
    global my_score
    global enemy_score
    global map
    global entities
    global entity_count
    global radar_cooldown
    global trap_cooldown
    global need_radar
    global total_visible_ores
    global safe_ores
    global turns_until_hq
    global closest_idle_to_hq
    global potential_diggers

    my_score, enemy_score = [int(i) for i in input().split()]

    total_visible_ores = 0
    safe_ores = 0
    stage.read()

    potential_diggers.clear()
    turns_until_hq = WIDTH
    closest_idle_to_hq = None
    entity_count, radar_cooldown, trap_cooldown = [int(i) for i in input().split()]
    entities.read(entity_count)

    need_radar = len(entities.where(type=RADAR)) < 16
