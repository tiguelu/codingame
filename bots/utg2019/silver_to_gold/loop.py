from .include import *


def closer_ores(bot):
    for cell in safe_ore_cells[bot:]:
        yield cell


while True:
    read_turn_data()

    if need_radar and closest_idle_to_hq is not None:
        if radar_cooldown == 0:
            if closest_idle_to_hq.x == 0:
                closest_idle_to_hq.reset_role(radar)
                if closest_idle_to_hq.role is not None:
                    potential_diggers.remove(closest_idle_to_hq)
                    radar_cooldown = 6
            elif total_visible_ores < 5 and turns_until_hq*4 > closest_idle_to_hq.x:
                closest_idle_to_hq.reset_role(forced_radar)
                potential_diggers.remove(closest_idle_to_hq)
                radar_cooldown = 6
        # elif total_visible_ores == 0 radar_cooldown <= turns_until_hq:
        #     if total_visible_ores < 5:
        #         closest_idle_to_hq.reset_role(forced_radar)
        #         potential_diggers.remove(closest_idle_to_hq)
        #         radar_cooldown = 6

    if safe_ores == 0:
        for bot in potential_diggers:
            bot.reset_role(noores)
    elif safe_ores < len(potential_diggers):
        for cell in safe_ore_cells:
            if cell in potential_traps.values():
                continue
            dists = sorted([(bot.dist(cell), bot.id) for bot in potential_diggers])
            for dist, bot_id in dists[:cell.ore]:
                bot = entities[bot_id]
                potential_diggers.remove(bot)
                bot.reset_role(ore, cell=cell)
        for bot in potential_diggers:
            bot.reset_role(noores)
    else:
        closest_ore = {bot: closer_ores(bot) for bot in potential_diggers}
        bot_ores = {}
        while potential_diggers:
            bot_ores.clear()
            for bot in potential_diggers:
                cell = next(closest_ore[bot])
                bot_ores.setdefault(cell, []).append(bot)
            for cell in bot_ores:
                bots = bot_ores[cell]
                if cell.ore >= len(bots):
                    for bot in bots:
                        potential_diggers.remove(bot)
                        bot.reset_role(ore, cell=cell)
                else:
                    closest_bots = sorted([(bot.dist(cell), bot.id) for bot in bots])
                    for _, bot_id in closest_bots[:cell.ore]:
                        bot = entities[bot_id]
                        potential_diggers.remove(bot)
                        bot.reset_role(ore, cell=cell)

    for bot in entities.where(type=ME):
        bot.print_cmd()
