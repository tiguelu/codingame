from .mocky import *


class Cell(MockyCell):
    def __init__(self, *props, **kwargs):
        self.trap = False
        self.radar = False
        self.unsafe = False
        self.pot_diggers = []
        self.excl_diggers = []
        kwargs.setdefault('extra_fields', ['trap', 'radar', 'unsafe'])
        super().__init__(*props, **kwargs)
        self.ore = None

    def on_update(self):
        global total_visible_ores
        global safe_ores

        if self.future is not None:
            return

        self.pot_diggers.clear()
        self.excl_diggers.clear()
        if self.ore == '?':
            if self.prev.ore and not self.hole:
                # This could happen when we lose a radar
                self.ore = self.prev.ore
                total_visible_ores += self.ore
                if not self.unsafe:
                    safe_ores += self.ore
            elif self.prev.ore == 0:
                # This is not necessarily true, but very likely
                self.ore = 0
            else:
                self.ore = None
        else:
            total_visible_ores += self.ore
            if not self.unsafe:
                safe_ores += self.ore
