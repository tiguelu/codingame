from .next_radar import *
from .role_ore import *


def radar(bot):
    if bot.item == NONE:
        if bot.x == 0:
            next_radar = bot.role_data.get('next', next_radar_max_coverage_min_dist)
            radar_loc = next_radar(bot)
            if radar_loc is None:
                return bot.clear_role()
            potential_radars[bot.id] = radar_loc
            return bot.request("RADAR", "Ojos que no ven")
        else:
            return bot.move(0, bot.y, '¡Adiós!')
    elif bot.item == RADAR:
        return bot.dig(*potential_radars[bot.id], 'Veo veo')
    elif bot.item == ORE:
        return bot.move(0, bot.y, 'Más')


def forced_radar(bot):
    if bot.item == NONE:
        if bot.x != 0:
            return bot.move(0, bot.y, '¡Adiós!')
        else:
            return bot.reset_role(radar)
    else:
        for cell in stage[bot:].where(unsafe=False):
            return bot.dig(cell.x, cell.y, 'Me obligan')