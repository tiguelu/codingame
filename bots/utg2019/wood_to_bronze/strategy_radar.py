from .definitions.imports_globals import *
from .next_radar import *
from .strategy_place_and_dig import *


def strategy_place_radars(robot, next_radar=optim_next_radar):
    next_radar_x, next_radar_y = next_radar()
    if next_radar_x is None:
        # No more radar to place
        return strategy_dig(robot)

    if robot.item == RADAR:
        # Carrying a radar
        return robot.dig(next_radar_x, next_radar_y, 'Veo veo')
    else:
        # Need to grab a radar or drop ore in HQ
        if robot.x == 0:
            if radar_cooldown == 0:
                # We're in HQ and there's an available radar or trap
                return robot.request("RADAR")
            else:
                return robot.wait('No sin mi radar')
        elif robot.item == NONE:
            chosen = None
            quick_mine_range = [point for point in closer_coords_from(robot.x, robot.y, 4)]

            for ore_point in stage.ore_points:
                cell = stage.items[ore_point[X_COORD]][ore_point[Y_COORD]]
                if cell.hole:
                    continue
                elif (ore_point[0], ore_point[1]) in quick_mine_range:
                    chosen = ore_point
                    break

            if chosen is not None:
                chosen[CAN_ORE] -= 1
                return robot.dig(chosen[X_COORD], chosen[Y_COORD], 'Soy minero')
            else:
                return robot.move(0, robot.y, 'Antojo de radar')

        else:
            # Robot needs to drop ore
            return robot.move(0, robot.y, 'Antojo de radar')
