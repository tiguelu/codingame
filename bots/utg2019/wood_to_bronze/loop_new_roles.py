from .include import *

robot_radar_far = None
robot_radar_middle = None
robot_trap = None

avail_bots = []

while True:
    read_turn_data()
    if not avail_bots:
        avail_bots = entities.mine.copy()
    # radar_placed = False
    # trap_placed = False

    ######
    # Robot radar far role
    if robot_radar_far is not None:
        if robot_radar_far.x == -1:
            # Our robot died, cleaning
            robot_radar_far = None
        else:
            strategy_place_radars(robot_radar_far)

    if robot_radar_far is None:
        # Choose closer robot to furthest radar position
        min_dist = math.inf
        for robot_id in avail_bots:
            robot = entities.items[robot_id]
            dist = robot.dist(25, 4)
            if dist < min_dist:
                robot_radar_far = robot
        avail_bots.remove(robot_radar_far.id)
        strategy_place_radars(robot_radar_far)

    ######
    # Robot radar middle role
    if robot_radar_middle is not None:
        if robot_radar_middle.x == -1:
            # Our robot died, cleaning
            robot_radar_middle = None
        else:
            strategy_place_radars(robot_radar_middle, next_radar=middle_next_radar)

    if robot_radar_middle is None:
        # Choose closer robot to middle radar position
        min_dist = math.inf
        for robot_id in avail_bots:
            robot = entities.items[robot_id]
            dist = robot.dist(14, 7)
            if dist < min_dist:
                robot_radar_middle = robot
        avail_bots.remove(robot_radar_middle.id)
        strategy_place_radars(robot_radar_middle, next_radar=middle_next_radar)

    ######
    # Robot trap role
    if robot_trap is not None:
        if robot_trap.x == -1:
            # Our robot died, cleaning
            robot_trap = None
        else:
            strategy_dig1_and_trap(robot_trap)

    if robot_trap is None:
        # Choose first avail robot for placing traps
        if avail_bots:
            robot_trap = entities.items[avail_bots[0]]
            avail_bots.remove(robot_trap.id)
            strategy_dig1_and_trap(robot_trap)

    for i in range(5):
        robot = entities.my_robot(i)
        if not robot.cmd:
            strategy_dig(robot)

    # for i in range(5):
    #     robot = entities.my_robot(i)
    #     if not radar_placed:
    #         status = strategy_place_radars(robot)
    #         radar_placed = status == NOT_DEAD
    #     elif not trap_placed:
    #         status = strategy_dig1_and_trap(robot)
    #         trap_placed = status == NOT_DEAD
    #     else:
    #         strategy_dig(robot)

    for i in range(5):
        robot = entities.my_robot(i)
        robot.print_cmd()
