from .definitions.imports_globals import *
from .next_radar import *
from .definitions.stage import *


def neighbours_x(start_x):
    if start_x == 0:
        start_x = 3
    while start_x < WIDTH-1:
        yield start_x
        start_x += 1


def strategy_dig(robot):
    if robot.item == ORE:
        # Go back to HQ
        return robot.move(0, robot.y, 'Morriña')

    # We have no ore, lets try and collect some
    min_dist = math.inf
    chosen = None
    for ore_point in stage.ore_points:
        dist = robot.dist(ore_point[X_COORD], ore_point[Y_COORD])
        cell = stage.items[ore_point[X_COORD]][ore_point[Y_COORD]]
        if cell.item == TRAP:
            continue
        if dist < min_dist and ore_point[CAN_ORE] > 0 and cell.hole == 0:
            min_dist = dist
            chosen = ore_point
    if chosen is not None:
        chosen[CAN_ORE] -= 1
        return robot.dig(chosen[X_COORD], chosen[Y_COORD], 'Orr Orr')

    # At this point we either don't know where to dig or there's no ore
    # If there are no radars yet, follow the first radar guy
    found_radar = False
    for cell in stage:
        found_radar = cell.item == RADAR
        break
    if not found_radar:
        # No radars yet in the stage, following first guy carrying a radar
        for bot_id in entities.mine:
            bot = entities.items[bot_id]
            if bot.item == RADAR:
                return robot.move(bot.x, bot.y, "¡Espérame!")

    start_x = max(robot.x, 1)
    start_y = max(robot.y, 1)
    for new_x, new_y in closer_coords_from(start_x, start_y, 14):
        cell = stage.items[new_x][new_y]
        if cell.ore is None and cell.hole == 0:
            return robot.dig(new_x, new_y, "Todo me vale")
    # By default...
    return robot.wait('Me aburro')

