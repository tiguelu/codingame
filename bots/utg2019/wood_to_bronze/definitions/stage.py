from .imports_globals import *


def coords_from(x, y, dist):
    first_inc_x = max(-dist, -x)
    last_inc_x = min(dist, WIDTH - x - 1)
    for inc_x in range(first_inc_x, last_inc_x + 1):
        first_inc_y = max(abs(inc_x) - dist, -y)
        last_inc_y = min(dist - abs(inc_x), HEIGHT - y - 1)
        for inc_y in range(first_inc_y, last_inc_y + 1):
            yield x + inc_x, y + inc_y


def closer_coords_from(x, y, dist):
    def in_range(x, y):
        return WIDTH > x >= 0 and HEIGHT > y >= 0

    if dist >= 0 and in_range(x, y):
        yield x, y
    for cur_dist in range(1, dist + 1):
        for turn in (1, -1):
            for x_dist in range(turn*cur_dist, -turn*cur_dist, -turn):
                cur_x = x + x_dist
                cur_y = y + turn*(cur_dist - abs(x_dist))
                if in_range(cur_x, cur_y):
                    yield cur_x, cur_y


class Stage(EntityIter):
    def __init__(self, width, height):
        self.fields = ['x', 'y', 'ore', 'hole', 'item', 'my_hole']
        self.width = width
        self.height = height
        self.ore_points = []
        items = []
        for x in range(width):
            items.append([])
            for y in range(height):
                dummy_values = [x, y, None, None, None, None]
                cell = Entity(*self.fields, values=dummy_values)
                items[x].append(cell)
        super().__init__(items=items)

    def update(self, ):
        self.ore_points = []
        for y in range(self.height):
            row = input().split()
            for x in range(self.width):
                ore = row[2 * x]
                ore = int(ore) if ore.isnumeric() else None
                hole = int(row[2 * x + 1])
                # Trying to find out if the enemy is making holes
                if hole and self.items[x][y].my_hole is None:
                    # There is a new hole appearing at this position
                    self.items[x][y].my_hole = False
                    for bot_id in entities.mine:
                        if bot_id not in entities.prev:
                            continue
                        prev_bot = entities.prev[bot_id]
                        if prev_bot.cmd == "DIG" and prev_bot.dist(x, y) == 1:
                            self.items[x][y].my_hole = True
                elif ore and self.items[x][y].ore and ore < self.items[x][y].ore:
                    # Ore has decreased, checking if we digged or the enemy did
                    ore_dif = self.items[x][y].ore - ore
                    my_new_ore = 0
                    for bot_id in entities.mine:
                        if bot_id not in entities.prev:
                            continue
                        bot = entities.items[bot_id]
                        prev_bot = entities.prev[bot_id]
                        if prev_bot.item != ORE and bot.item == ORE:
                            my_new_ore += 1
                    if my_new_ore != ore_dif:
                        # Enemies digged up ore!
                        self.items[x][y].my_hole = False
                self.items[x][y].ore = ore
                self.items[x][y].hole = hole
                self.items[x][y].item = None
                if ore and ore > 0:
                    self.ore_points.append([x, y, ore])
        for item in entities:
            if item.type in (RADAR, TRAP):
                self.items[item.x][item.y].item = item.type

    def __iter__(self):
        for rows in self.items:
            for cell in rows:
                if cell.has_props(**self.props_filter) and cell.has_dif_props(**self.props_dif_filter):
                    yield cell


stage = Stage(WIDTH, HEIGHT)