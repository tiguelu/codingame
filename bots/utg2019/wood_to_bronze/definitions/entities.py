from .imports_globals import *


def is_int(a):
    try:
        int(a)
    except ValueError:
        return False
    return True


class Entity:
    def __init__(self, *fields, values=None):
        self._fields = fields
        self.cmd = None
        self.cmd_options = None
        if values is None:
            values = [int(value) if is_int(value) else value
                      for value in input().split()]
        for k, v in zip(fields, values):
            setattr(self, k, v)

    def update(self, values):
        for k, v in zip(self._fields, values):
            setattr(self, k, v)

    def has_props(self, **properties):
        for prop, val in properties.items():
            if prop not in self.__dict__:
                return False
            if self.__getattribute__(prop) != val:
                return False
        else:
            return True

    def has_dif_props(self, **properties):
        for prop, val in properties.items():
            if prop not in self.__dict__:
                return False
            if self.__getattribute__(prop) == val:
                return False
        else:
            return True

    def dist(self, x, y=None):
        if type(x) == Entity:
            x, y = x.x, x.y
        return math.sqrt((self.x-x)**2 + (self.y-y)**2)

    def set_cmd(self, cmd, *options):
        if self.x == -1:
            self.cmd = "WAIT"
            self.cmd_options = [f"I'm dead bot {self.id}"]
            return DEAD_BOT
        else:
            self.cmd = cmd
            self.cmd_options = options
            return NOT_DEAD

    def print_cmd(self):
        print(self.cmd, *self.cmd_options)

    def move(self, *options):
        return self.set_cmd('MOVE', *options)

    def wait(self, *options):
        return self.set_cmd("WAIT", *options)

    def dig(self, *options):
        return self.set_cmd("DIG", *options)

    def request(self, *options):
        return self.set_cmd("REQUEST", *options)

    def __str__(self):
        value_pairs = []
        for field in self._fields:
            value_pairs.append(f'{field}={self.__dict__[field]}')
        return ', '.join(value_pairs)


class EntityIter:
    def __init__(self, props_filter=None, props_dif_filter=None, items=None):
        self.props_filter = {} if props_filter is None else props_filter
        self.props_dif_filter = {} if props_dif_filter is None else props_dif_filter
        self.items = [] if items is None else items

    def read(self, *properties, values=None):
        item = Entity(*properties, values)
        self.items.append(item)
        return item

    def where(self, **props_filter):
        new_props_filter = self.props_filter.copy()
        new_props_filter.update(props_filter)
        return EntityIter(props_filter=new_props_filter,
                         props_dif_filter=self.props_dif_filter, items=self.items)

    def wherenot(self, **props_dif_filter):
        new_props_filter = self.props_dif_filter.copy()
        new_props_filter.update(props_dif_filter)
        return EntityIter(props_filter=self.props_filter,
                         props_dif_filter=new_props_filter, items=self.items)

    def __iter__(self):
        items = self.items.values() if type(self.items) == dict else self.items
        for item in items:
            if item.has_props(**self.props_filter) and item.has_dif_props(**self.props_dif_filter):
                yield item

    def __str__(self):
        items_strings = []
        for item in self:
            items_strings.append(f'<{item}>')
        return '\n'.join(items_strings)


class EntityDictIter(EntityIter):
    def __init__(self):
        super().__init__(items={})

    def read(self, *properties, id_key='id'):
        item = Entity(*properties)
        self.items[getattr(item, id_key)] = item
        return item

    def __getitem__(self, id_value):
        if id_value not in self.items:
            return None
        return self.items[id_value]


class Entities(EntityDictIter):
    def __init__(self):
        super().__init__()
        self.prev = {}
        self.mine = []
        self.enemy = []
        self.radars = []
        self.traps = []

    def read(self, n):
        props = ['id', 'type', 'x', 'y', 'item']
        self.prev = self.items
        self.items = {}
        self.radars = []
        self.traps = []
        for i in range(n):
            entity = super().read(*props)
            if entity.type == MINE:
                if len(self.mine) < 5:
                    self.mine.append(entity.id)
            elif entity.type == ENEMY:
                if len(self.enemy) < 5:
                    self.enemy.append(entity.id)
            elif entity.type == RADAR:
                self.radars.append(entity.id)
            elif entity.type == TRAP:
                self.traps.append(entity.id)

    def my_robot(self, idx):
        entity_id = self.mine[idx]
        return self.items[entity_id]

    def my_enemy(self, idx):
        entity_id = self.enemy[idx]
        return self.items[entity_id]


entities = Entities()
