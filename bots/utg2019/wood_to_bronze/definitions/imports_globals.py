import math
import sys
import random


def dbg(*s):
    print(*s, file=sys.stderr)


WIDTH, HEIGHT = [int(i) for i in input().split()]
RADAR_RANGE = 4

# Item/Entity type
NONE = -1
MINE = 0
ENEMY = 1
RADAR = 2
TRAP = 3
ORE = 4

# ORE points indexes
X_COORD = 0
Y_COORD = 1
CAN_ORE = 2

# Strategy returns
DEAD_BOT = -1
NOT_DEAD = 1

# Global vars
my_score = None
enemy_score = None
radar_cooldown = None
trap_cooldown = None
entities = None
stage = None