from .definitions.imports_globals import *


def read_turn_data():
    global my_score
    global enemy_score
    global map
    global entities
    global entity_count
    global radar_cooldown
    global trap_cooldown

    my_score, enemy_score = [int(i) for i in input().split()]

    stage.update()

    entity_count, radar_cooldown, trap_cooldown = [int(i) for i in input().split()]
    dbg('entities', entity_count, 'radar_cd', radar_cooldown, 'trap_cd', trap_cooldown)
    entities.read(entity_count)
