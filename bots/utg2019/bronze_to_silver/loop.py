from .include import *

while True:
    read_turn_data()
    far_radar_placed = False
    middle_radar_placed = False
    trap_placed = False

    for robot in entities.where(type=MINE):
        if not middle_radar_placed:
            status = strategy_place_radars(robot, next_radar=middle_next_radar)
            middle_radar_placed = status == NOT_DEAD
        elif not far_radar_placed:
            if len(entities.where(type=RADAR)) >= 15:
                strategy_dig(robot)
                far_radar_placed = True
            else:
                status = strategy_place_radars(robot)
                far_radar_placed = status == NOT_DEAD
        elif not trap_placed:
            status = strategy_dig1_and_trap(robot)
            trap_placed = status == NOT_DEAD
        else:
            strategy_dig(robot)

    for robot in entities.where(type=MINE):
        robot.print_cmd()
