from .definitions.imports_globals import *


def closer_coords_from_point(x, y, dist):
    def in_range(x, y):
        return WIDTH > x >= 0 and HEIGHT > y >= 0
    if dist >= 0:
        yield x, y
    for cur_dist in range(1, dist + 1):
        for turn in (1, -1):
            for x_dist in range(turn*cur_dist, -turn*cur_dist, -turn):
                cur_x = x + x_dist
                cur_y = y + turn*(cur_dist - abs(x_dist))
                if in_range(cur_x, cur_y):
                    yield cur_x, cur_y


def radar_oscardist_generator():
    # for x in range(5, WIDTH, 9):
    for x in range(1, WIDTH, 8):
        # for y in range(4, HEIGHT, 9):
        for y in range(4, HEIGHT, 10):
            yield x, y
    # for x in range(1, WIDTH, 9):
    for x in range(5, WIDTH, 8):
        # for y in range(-1, HEIGHT, 9):
        for y in range(0, HEIGHT, 9):
            if x > 0:
                yield x, y


def trap_dist_generator(fromx, tox, fromy, toy):
    # for x in range(5, WIDTH, 9):
    for x in range(fromx, tox+1):
        # for y in range(4, HEIGHT, 9):
        for y in range(0, 15, 2):
            yield x, y
    for x in range(fromy, toy+1):
        for y in range(1, 15, 2):
            yield x, y


def radar_optim_dist_area_covered(robot):
    def score(point):
        dist = robot.dist(*point)
        area = 0
        for i in closer_coords_from_point(*point, RADAR_RANGE):
            area += 1

        return area, dist

    return sorted(RADAR_LOCS, key=score, reverse=True)

# RADAR_LOCS and RADAR_LOCS_OPTIM can be calculated like:
# RADAR_LOCS = [point for point in radar_generator()]
# RADAR_OPTIM = radar_optim_dist_area_covered(robot)



RADAR_LOCS = [
    (1, 4), (1, 14), (9, 4), (9, 14), (17, 4), (17, 14), (25, 4), (25, 14), (5, 0),
    (5, 9), (13, 0), (13, 9), (21, 0), (21, 9), (29, 0), (29, 9),
]

RADAR_OPTIM_FROM_00 = [
    (25, 4), (21, 9), (17, 4), (9, 4), (13, 9), (5, 9), (1, 4), (29, 9), (21, 0),
    (25, 14), (13, 0), (5, 0), (17, 14), (9, 14), (1, 14), (29, 0)
]

ENEMY_RADAR_LOCS = [
    (5, 3), (5, 11), (15, 3), (15, 11), (25, 4), (25, 11), (10, 14), (20, 14)
]

# TRAP_LOCS = [point for point in trap_dist_generator()]


def radar_optim_dist_area_covered_mid():
    def score(point):
        middle = (14, 7)
        dist = math.sqrt((middle[0] - point[0])**2 + (middle[1] - point[1])**2)
        area = 0
        for _ in closer_coords_from_point(*point, RADAR_RANGE):
            area += 1
        return area, 1/dist
    return sorted(RADAR_LOCS, key=score, reverse=True)


def radar_optim_dist_area_covered_rand():
    def score(point):
        dist = random.randint(1, 100)
        area = 0
        for _ in closer_coords_from_point(*point, RADAR_RANGE):
            area += 1
        return area, dist
    return sorted(RADAR_LOCS, key=score, reverse=True)


def optim_next_radar_robot_broken(robot):
    # Radar points with bigger area and further away, are provided first
    for point in radar_optim_dist_area_covered(robot):
        if stage.items[point[0]][point[1]].item != RADAR:
            return point
    else:
        return None, None


def optim_next_radar():
    # Radar points with bigger area and further away, are provided first
    for point in RADAR_OPTIM_FROM_00:
        if stage.items[point[0]][point[1]].item != RADAR:
            return point
    else:
        return None, None


def middle_next_radar():
    # Radar points with bigger area and closer to the middle, are provided first
    for point in radar_optim_dist_area_covered_mid():
        if stage.items[point[0]][point[1]].item != RADAR:
            return point
    else:
        return None, None


def random_next_radar(robot):
    # Radar points with bigger area, are provided randomly
    for point in radar_optim_dist_area_covered(robot):
        if stage.items[point[0]][point[1]].item != RADAR:
            return point
    else:
        return None, None


