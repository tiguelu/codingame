from .definitions.imports_globals import *
from .definitions.stage import *


def strategy_dig1_and_trap(robot):
    if robot.item != TRAP:
        # We have no trap or we are carrying ore, lets grab a trap to HQ
        # and possibly drop ore
        if robot.x == 0:
            # We are in HQ
            if trap_cooldown == 0:
                # We're in HQ and there's an available radar or trap
                return robot.request("TRAP")
            else:
                # Waiting for a trap to be available in HQ
                return robot.wait("Sin trampa ni cartón")
        else:
            # Robot needs to drop ore or go back to HQ
            return robot.move(0, robot.y, "Morriña de trampa")
    else:
        # At this point the robot is carrying a trap and needs to drop it
        min_dist_1 = math.inf
        min_dist_2 = math.inf
        chosen_1 = None
        chosen_2 = None
        for ore_point in stage.ore_points:
            cell = stage.items[ore_point[X_COORD]][ore_point[Y_COORD]]
            dist = robot.dist(ore_point[X_COORD], ore_point[Y_COORD])
            if ore_point[CAN_ORE] == 2 and dist < min_dist_1 and cell.hole == 0:
                min_dist_1 = dist
                chosen_1 = ore_point
            if ore_point[CAN_ORE] > 2 and dist < min_dist_2 and cell.hole == 0:
                min_dist_2 = dist
                chosen_2 = ore_point
        if chosen_1 is not None:
            chosen_1[CAN_ORE] -= 1
            return robot.dig(chosen_1[X_COORD], chosen_1[Y_COORD], "Morirás")
        elif chosen_2 is not None:
            chosen_2[CAN_ORE] -= 1
            return robot.dig(chosen_2[X_COORD], chosen_2[Y_COORD], "Morirás más")

    # At this point we either don't know where to leave the trap
    # Try to put it in some non-holed place near to the robot but not enough to
    # kill it if it explodes
    for new_x, new_y in closer_coords_from(robot.x, robot.y, 4):
        if new_x == 0 or new_y == 0:
            continue
        if not stage.items[new_x][new_y].hole:
            return robot.dig(new_x, new_y, 'Viva el queso')

    # Default wait
    return robot.wait("Me aburro sin trampa")
