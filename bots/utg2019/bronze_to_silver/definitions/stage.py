from .imports_globals import *
from .mocky import *


def coords_from(x, y, dist):
    first_inc_x = max(-dist, -x)
    last_inc_x = min(dist, WIDTH - x - 1)
    for inc_x in range(first_inc_x, last_inc_x + 1):
        first_inc_y = max(abs(inc_x) - dist, -y)
        last_inc_y = min(dist - abs(inc_x), HEIGHT - y - 1)
        for inc_y in range(first_inc_y, last_inc_y + 1):
            yield x + inc_x, y + inc_y


def closer_coords_from(x, y, dist):
    def in_range(x, y):
        return WIDTH > x >= 0 and HEIGHT > y >= 0

    if dist >= 0 and in_range(x, y):
        yield x, y
    for cur_dist in range(1, dist + 1):
        for turn in (1, -1):
            for x_dist in range(turn*cur_dist, -turn*cur_dist, -turn):
                cur_x = x + x_dist
                cur_y = y + turn*(cur_dist - abs(x_dist))
                if in_range(cur_x, cur_y):
                    yield cur_x, cur_y


class Cell(MockyCell):
    def __init__(self, *props, **kwargs):
        self.item = None
        self.my_hole = None
        if 'extra_fields' not in kwargs:
            kwargs['extra_fields'] = ['item', 'my_hole']
        super().__init__(*props, **kwargs)

    def _read_input(self):
        # Reading ore and hole from input row
        ore = self.row[2 * self.x]
        ore = int(ore) if ore.isnumeric() else None
        hole = int(self.row[2 * self.x + 1])
        self.item = None
        # Trying to find out if the enemy is making holes
        if hole and self.my_hole is None:
            # There is a new hole appearing at this position
            self.my_hole = False
            for bot in entities.where(type=MINE):
                if bot.prev is None:
                    continue
                if bot.prev.cmd == "DIG" and bot.prev.dist(self.x, self.y) == 1:
                    self.my_hole = True
        elif ore and self.ore and ore < self.ore:
            # Ore has decreased, checking if we digged or the enemy did
            ore_dif = self.ore - ore
            my_new_ore = 0
            for bot in entities.where(type=MINE):
                if bot.prev is None:
                    continue
                if bot.prev.item != ORE and bot.item == ORE:
                    my_new_ore += 1
            if my_new_ore != ore_dif:
                # Enemies digged up ore!
                self.my_hole = False
        return ore, hole


class Stage(Mocky2DIter):
    def __init__(self, *args, **kwargs):
        self.ore_points = []
        super().__init__(*args, **kwargs)

    def post_read(self):
        for item in entities:
            if item.type in (RADAR, TRAP):
                self.items[item.x][item.y].item = item.type
        self.ore_points = []
        for cell in self:
            if cell.ore and cell.ore > 0:
                self.ore_points.append([cell.x, cell.y, cell.ore])


stage = Stage('ore', 'hole', width=WIDTH, height=HEIGHT, keep=0, new_from=Cell)
