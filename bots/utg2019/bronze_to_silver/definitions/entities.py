from .imports_globals import *
from .mocky import *


class Entity(Mocky):
    def __init__(self, *args, **kwargs):
        self.cmd = None
        self.cmd_options = None
        if 'extra_fields' not in kwargs:
            kwargs['extra_fields'] = ['cmd', 'cmd_options']
        super().__init__(*args, **kwargs)
        if self.type in (RADAR, TRAP):
            self._keep = 0

    def set_cmd(self, cmd, *options):
        if self.x == -1:
            self.cmd = "WAIT"
            self.cmd_options = [f"I'm dead bot {self.id}"]
            return DEAD_BOT
        else:
            self.cmd = cmd
            self.cmd_options = options
            return NOT_DEAD

    def print_cmd(self):
        print(self.cmd, *self.cmd_options)

    def move(self, *options):
        return self.set_cmd('MOVE', *options)

    def wait(self, *options):
        return self.set_cmd("WAIT", *options)

    def dig(self, *options):
        return self.set_cmd("DIG", *options)

    def request(self, *options):
        return self.set_cmd("REQUEST", *options)


entities = MockyDictIter('id', 'type', 'x', 'y', 'item', new_from=Entity, keep=1)
