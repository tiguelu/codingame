from .imports_globals import *


def is_int(a):
    try:
        int(a)
    except ValueError:
        return False
    return True


class Mocky:
    def __init__(self, *fields, values=None, extra_fields=None, keep=1):
        extra_fields = extra_fields if extra_fields is not None else []
        self._fields = list(fields)
        self._fields.extend(extra_fields)
        self._extra_fields = len(extra_fields)
        self._keep = keep
        self.prev = None
        self._write(values)

    def _read_input(self):
        values = [int(value) if is_int(value) else value
                  for value in input().split()]
        return values

    def _write(self, values):
        values = self._read_input() if values is None else values
        for k, v in zip(self._fields, values):
            setattr(self, k, v)

    def values(self, extra=True):
        last = None if extra else -self._extra_fields
        for field in self._fields[:last]:
            yield getattr(self, field)

    def update(self, new=None):
        if isinstance(self.prev, Mocky):
            self.prev.update(new=self.values())
        elif self._keep > 0:
            self.prev = self.__class__(
                *self._fields[:-self._extra_fields],
                extra_fields=self._fields[-self._extra_fields:],
                values=self.values(),
                keep=self._keep - 1)
        if isinstance(new, Mocky):
            new = new.values(extra=False)
        self._write(new)

    def has_props(self, **properties):
        for prop, val in properties.items():
            if prop not in self.__dict__:
                return False
            if self.__getattribute__(prop) != val:
                return False
        else:
            return True

    def has_dif_props(self, **properties):
        for prop, val in properties.items():
            if prop not in self.__dict__:
                return False
            if self.__getattribute__(prop) == val:
                return False
        else:
            return True

    def dist(self, x, y=None):
        if isinstance(x, Mocky):
            x, y = x.x, x.y
        elif isinstance(x, list):
            x, y = x[0:2]
        return math.sqrt((self.x-x)**2 + (self.y-y)**2)

    def __str__(self):
        value_pairs = []
        for field in self._fields:
            value_pairs.append(f'{field}={self.__dict__[field]}')
        return ', '.join(value_pairs)


class MockyIter:
    def __init__(self, *props, keep=0, new_from=Mocky,
                 props_filter=None, props_dif_filter=None, parent=None):
        self.props = props
        self.keep = keep
        self.new_from = new_from
        self.props_filter = {} if props_filter is None else props_filter
        self.props_dif_filter = {} if props_dif_filter is None else props_dif_filter
        self.items = [] if parent is None else parent.items

    def read(self, n):
        if n == len(self.items):
            for i in range(n):
                self.items[i].update()
        else:
            self.items.clear()
            for _ in range(n):
                item = self.new_from(*self.props, keep=self.keep)
                self.items.append(item)

    def where(self, **props_filter):
        new_props_filter = self.props_filter.copy()
        new_props_filter.update(props_filter)
        return self.__class__(props_filter=new_props_filter,
                              props_dif_filter=self.props_dif_filter, parent=self)

    def wherenot(self, **props_dif_filter):
        new_props_filter = self.props_dif_filter.copy()
        new_props_filter.update(props_dif_filter)
        return self.__class__(props_filter=self.props_filter,
                              props_dif_filter=new_props_filter, parent=self)

    def filters(self, item):
        return (
            item.has_props(**self.props_filter)
            and item.has_dif_props(**self.props_dif_filter)
        )

    def __iter__(self):
        for item in self.items:
            if self.filters(item):
                yield item

    def __len__(self):
        length = 0
        for item in self.items:
            if self.filters(item):
                length += 1
        return length

    def __str__(self):
        items_strings = []
        for item in self:
            items_strings.append(f'<{item}>')
        return '\n'.join(items_strings)


class MockyDictIter(MockyIter):
    def __init__(self, *props, **kwargs):
        super().__init__(*props, **kwargs)
        if 'parent' not in kwargs:
            self.items = {}

    def read(self, n):
        if not self.items:
            for _ in range(n):
                item = self.new_from(*self.props, keep=self.keep)
                self.items[item.id] = item
        else:
            non_updated_ids = list(self.items.keys())
            for _ in range(n):
                item = self.new_from(*self.props, keep=self.keep)
                if item.id in self.items:
                    self.items[item.id].update(new=item)
                    non_updated_ids.remove(item.id)
                else:
                    self.items[item.id] = item
            for item_id in non_updated_ids:
                del self.items[item_id]

    def __getitem__(self, id_value):
        if id_value not in self.items:
            return None
        return self.items[id_value]

    def __iter__(self):
        for item in self.items.values():
            if self.filters(item):
                yield item

    def __len__(self):
        length = 0
        for item in self.items.values():
            if self.filters(item):
                length += 1
        return length


class MockyCell(Mocky):
    def __init__(self, *props, **kwargs):
        self.x = kwargs.pop('x', None)
        self.y = kwargs.pop('y', None)
        self.row = kwargs.pop('row', None)
        super().__init__(*props, **kwargs)

    def _read_input(self):
        """
        To be overwritten.
        In a 2D map input will be provided in a row.
        Its cell data gets extracted here.
        :return: Cell data
        """
        return ['?'] * len(self._fields)


class Mocky2DIter(MockyIter):
    def __init__(self, *props, **kwargs):
        if 'parent' in kwargs:
            self.width = kwargs['parent'].width
            self.height = kwargs['parent'].height
        else:
            self.width = kwargs.pop('width')
            self.height = kwargs.pop('height')
        if 'new_from' not in kwargs:
            # new_from must be MockyCell or a subclass
            kwargs['new_from'] = MockyCell
        super().__init__(*props, **kwargs)
        if not self.items:
            for x in range(self.width):
                self.items.append([])
                for y in range(self.height):
                    # Initializing empty cells
                    self.items[x].append(
                        self.new_from(
                            *self.props,
                            keep=self.keep,
                            values=[None]*len(self.props),
                            x=x,
                            y=y,
                        )
                    )

    def post_read(self):
        pass

    def read(self):
        # This function assumes 2D cell data is provided per row
        for y in range(self.height):
            row = input().split()
            for x in range(self.width):
                self.items[x][y].row = row
                self.items[x][y].update()
        self.post_read()

    def __iter__(self):
        for row in self.items:
            for item in row:
                if self.filters(item):
                    yield item

    def __len__(self):
        length = 0
        for row in self.items:
            for item in row:
                if self.filters(item):
                    length += 1
        return length
