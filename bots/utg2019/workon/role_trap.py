from .next_trap import *
from .next_radar import *
from .role_ore import *


def trap(bot):
    if bot.item == NONE:
        if bot.x == 0:
            bot.role_data['trapper'] = False
            return bot.request("TRAP", "I am Íñigo Montoya")
        else:
            if bot.role_data.get('trapper', None):
                return bot.move(0, bot.y, 'A cazar')
            else:
                return bot.clear_role()
    elif bot.item == TRAP:
        for trap_loc in enemy_radar_predictor():
            if trap_loc in RADAR_LOCS:
                continue
            trap_cell = stage[trap_loc]
            potential_traps[bot.id] = trap_cell
            return bot.dig(*trap_loc, f"¿Morirás?")
        # for cell in safe_ore_cells[bot:].eval('ore >= 2'):
        #     if (cell.x, cell.y) in RADAR_LOCS:
        #         continue
        #     potential_traps[bot.id] = cell
        #     return bot.dig(cell.x, cell.y, f"Morirás con {cell.ore}")

        # At this point we either have no visibility of places with ore, or
        # all ore cells have been dug or have less than 2 ores.
        # We'll try to put it in some non-holed place near the robot.
        for cell in stage[bot:].eval("hole == 0 and x > 2"):
            return bot.dig(cell.x, cell.y, 'Viva el queso')
    elif bot.item == ORE:
        return bot.move(0, bot.y, 'Vaya suerte')
    else:
        return bot.reset_clear()
