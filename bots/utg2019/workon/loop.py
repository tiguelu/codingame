from .include import *


def closer_ores(bot):
    for cell in safe_ore_cells[bot:]:
        yield cell


trapper = None
while True:
    read_turn_data()
    dbg(entities.where(type=ENEMY))
    if need_radar and closest_idle_to_hq is not None:
        if radar_cooldown == 0:
            if closest_idle_to_hq.x == 0:
                if safe_ores > 8:
                    closest_idle_to_hq.reset_role(radar, next=next_radar_bigger_min_dist)
                else:
                    closest_idle_to_hq.reset_role(radar)
                if closest_idle_to_hq.role is not None:
                    potential_diggers.remove(closest_idle_to_hq)
                    radar_cooldown = 6
            elif total_visible_ores < 5 and turns_until_hq*4 > closest_idle_to_hq.x:
                closest_idle_to_hq.reset_role(radar)
                potential_diggers.remove(closest_idle_to_hq)
                radar_cooldown = 6
        elif trap_cooldown == 0 and len(potential_enemy_radars):
            if trapper:
                trapper.reset_role(trap, trapper=True)
                trapper = None
            else:
                closest_idle_to_hq.reset_role(trap)
                potential_diggers.remove(closest_idle_to_hq)
            trap_cooldown = 6
        # elif total_visible_ores == 0 radar_cooldown <= turns_until_hq:
        #     if total_visible_ores < 5:
        #         closest_idle_to_hq.reset_role(forced_radar)
        #         potential_diggers.remove(closest_idle_to_hq)
        #         radar_cooldown = 6

    if turn == 1:
        dist_to_y7 = math.inf
        for bot in potential_diggers:
            if abs(7 - bot.y) < dist_to_y7:
                dist_to_y7 = abs(7 - bot.y)
                trapper = bot
        potential_diggers.remove(trapper)
        trapper.reset_role(noores, trapper=True)

    if safe_ores == 0:
        for bot in potential_diggers:
            bot.reset_role(noores)
    elif safe_ores < len(potential_diggers):
        for cell in safe_ore_cells:
            if cell in potential_traps.values():
                continue
            dists = sorted([(bot.dist(cell), bot.id) for bot in potential_diggers])
            for dist, bot_id in dists[:cell.ore]:
                bot = entities[bot_id]
                potential_diggers.remove(bot)
                bot.reset_role(ore, cell=cell)
        for bot in potential_diggers:
            bot.reset_role(noores)
    else:
        closest_ore = {bot: closer_ores(bot) for bot in potential_diggers}
        bot_ores = {}
        while potential_diggers:
            bot_ores.clear()
            for bot in potential_diggers:
                cell = next(closest_ore[bot])
                bot_ores.setdefault(cell, []).append(bot)
            for cell in bot_ores:
                bots = bot_ores[cell]
                if cell.ore >= len(bots):
                    for bot in bots:
                        potential_diggers.remove(bot)
                        bot.reset_role(ore, cell=cell)
                else:
                    closest_bots = sorted([(bot.dist(cell), bot.id) for bot in bots])
                    for _, bot_id in closest_bots[:cell.ore]:
                        bot = entities[bot_id]
                        potential_diggers.remove(bot)
                        bot.reset_role(ore, cell=cell)

    for bot in entities.where(type=ME):
        bot.print_cmd()
