from .instantiation import *


def in_range(xpos, ypos):
    return WIDTH > xpos >= 0 and HEIGHT > ypos >= 0


def pattern_boss(x, y):
    while x < WIDTH:
        if 6 <= y <= 8:
            x += 5
            y += 4
        elif y > 8:
            y -= 8
        elif y < 6:
            y += 4
            x += 5
        if not in_range(x, y):
            continue
        yield x, y


def pattern_pkon(x, y):
    while x < WIDTH:
        if 6 <= y <= 8:
            x += 4
            y += 5
        elif y > 8:
            y -= 10
        elif y < 6:
            y += 5
            x += 4
        if not in_range(x, y):
            continue
        yield x, y


def pattern_dallas(x, y):
    while x < WIDTH:
        if y < 9:
            x += 4
            y += 4
        else:
            y -= 8
        if not in_range(x, y):
            continue
        yield x, y


def pattern_us(x, y):
    while x < WIDTH:
        if y > 7:
            x += 4
            y -= 5
        else:
            y -= 5
            x += 4
        if not in_range(x, y):
            continue
        yield x, y


def enemy_radar_predictor():
    if turn < 5:
        potential_enemy_radars.sort(reverse=True, key=lambda c: c.x)

    # Determine best pattern
    scored_cells = []
    for cell in potential_enemy_radars:
        patterns = [pattern_boss, pattern_pkon, pattern_dallas]
        for predictor in patterns:
            score = 0
            for x, y in predictor(cell.x, cell.y):
                if stage[x, y].unsafe:
                    score += 1
            scored_cells.append((score, cell, predictor))
    scored_cells.sort(key=lambda i: i[0], reverse=True)

    for _, enemy_radar, predictor in scored_cells:
        for x, y in predictor(enemy_radar.x, enemy_radar.y):
            dbg('checking', x, y, predictor, enemy_radar)
            cell = stage[x, y]
            if cell.trap or cell.unsafe or cell.trapped:
                continue
            yield x, y
    dbg('nothing found')
    potential_enemy_radars.clear()
