import math
import sys


def dbg(*s):
    print(*s, file=sys.stderr)


WIDTH, HEIGHT = [int(i) for i in input().split()]
RADAR_RANGE = 4

# Item/Entity type
NONE = -1
ME = 0
ENEMY = 1
RADAR = 2
TRAP = 3
ORE = 4

# Global vars
my_score = None
enemy_score = None
radar_cooldown = None
trap_cooldown = None
entities = None
stage = None
turn = 0

potential_radars = {}
potential_traps = {}
potential_enemy_radars = []

need_radar = True
total_visible_ores = 0
safe_ores = 0
turns_until_hq = None
closest_idle_to_hq = None
potential_diggers = []

ore_cells = None
safe_ore_cells = None
