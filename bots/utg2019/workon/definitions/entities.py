from .mocky import *


class Entity(Mocky):
    def __init__(self, *args, **kwargs):
        self.itemized = False
        self.first_itemized = False
        self.role = None
        self.run_role = self.wait
        self.role_data = {}
        self.cmd = None
        self.cmd_options = None
        kwargs.setdefault('extra_fields', ['itemized', 'role', 'cmd', 'cmd_options'])
        super().__init__(*args, **kwargs)
        if self.type == RADAR:
            stage[self].radar = True
            self._keep = 0
        elif self.type == TRAP:
            stage[self].trap = True
            stage[self].trapped = True
            self._keep = 0

    def clear(self):
        if self.type == RADAR:
            stage[self].radar = False
        elif self.type == TRAP:
            stage[self].trap = False

    def on_update(self):
        global safe_ores
        global closest_idle_to_hq
        global turns_until_hq
        global turn
        global potential_enemy_radars

        if self.future:
            return
        if self.type == ME:
            if self.x == -1:
                return
            if self.item == NONE and not self.role_data.get('trapper', False):
                potential_diggers.append(self)
                if closest_idle_to_hq is None or closest_idle_to_hq.x > self.x:
                    closest_idle_to_hq = self

            if self.item == NONE and self.x == 0:
                turns_until_hq = 0
            elif self.item == ORE:
                this_turns_until_hq = math.ceil(self.x / 4)
                if this_turns_until_hq < turns_until_hq:
                    turns_until_hq = this_turns_until_hq
            elif self.prev and self.prev.cmd == "MOVE":
                if self.prev.cmd_options[0] == 0:
                    this_turns_until_hq = math.ceil(self.x / 4)
                    if this_turns_until_hq < turns_until_hq:
                        turns_until_hq = this_turns_until_hq

        elif self.type == ENEMY and self.prev:
            # TODO Uncomment if guessing is too permisive and remove itemized
            #   updating below
            # if self.x == 0:
            #     self.itemized = False
            if self.x == self.prev.x and self.y == self.prev.y:
                if self.x == 0:
                    # Enemy is getting radar or trap
                    self.itemized = True
                    if turn < 5:
                        self.first_itemized = True
                        dbg('first item', self, self.prev)
                elif self.itemized:
                    # Enemy might be digging: determine certain degree of unsafety
                    # TODO
                    #  if this is too permissive, just block any cells around
                    #  that have a hole
                    diggable = stage[self:1]
                    for cell in diggable.eval("prev.ore and ore and ore < prev.ore"):
                        cell.unsafe = True
                        self.itemized = False
                        if not cell.prev.unsafe:
                            safe_ores -= cell.ore
                        if self.first_itemized:
                            potential_enemy_radars.append(cell)
                            dbg('ore', potential_enemy_radars)
                    if self.itemized:
                        for cell in diggable.eval("hole and not prev.hole"):
                            cell.unsafe = True
                            self.itemized = False
                            if cell.ore and not cell.prev.unsafe:
                                safe_ores -= cell.ore
                            if self.first_itemized:
                                potential_enemy_radars.append(cell)
                                dbg('hole', potential_enemy_radars)
                    if self.itemized:
                        for cell in diggable.where(hole=1):
                            self.itemized = False
                            cell.unsafe = True
                            if cell.ore and not cell.prev.unsafe:
                                safe_ores -= cell.ore
                            if self.first_itemized:
                                potential_enemy_radars.append(cell)
                                dbg('else', potential_enemy_radars)
                    self.first_itemized = False

    def clear_role(self):
        self.role_data.clear()
        potential_radars[self.id] = None
        potential_traps[self.id] = None
        self.role = None
        self.run_role = self.wait

    def reset_role(self, role=None, **kwargs):
        if role is None:
            role = self.role
        self.clear_role()
        self.role = role
        self.role_data = kwargs
        if self.role is not None:
            method = type(self.reset_role)
            self.run_role = method(self.role, self)
        self.run_role()

    def print_cmd(self):
        if self.x == -1:
            self.run_role = self.dead
        if not self.cmd:
            self.run_role()
        print(self.cmd, *self.cmd_options)
        self.cmd = None

    def set_cmd(self, cmd, *options):
        self.cmd = cmd
        self.cmd_options = options

    def move(self, *options):
        return self.set_cmd('MOVE', *options)

    def wait(self, *options):
        return self.set_cmd("WAIT", *options)

    def dig(self, *options):
        return self.set_cmd("DIG", *options)

    def request(self, *options):
        return self.set_cmd("REQUEST", *options)

    def dead(self, *args):
        return self.wait(f"I'm dead bot {self.id}")
