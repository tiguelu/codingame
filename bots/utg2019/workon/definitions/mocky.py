from .imports_globals import *


def int_or_raw(a):
    try:
        return int(a)
    except ValueError:
        return a


class Mocky:
    def __init__(self, *fields, values=None, extra_fields=None, keep=1):
        extra_fields = extra_fields if extra_fields is not None else []
        self._fields = list(fields)
        self._fields.extend(extra_fields)
        self._extra_fields = len(extra_fields)
        self._keep = keep
        self.prev = None
        self.future = None
        self._write(values)

    def _read_input(self):
        values = [int_or_raw(value) for value in input().split()]
        return values

    def _write(self, values):
        values = self._read_input() if values is None else values
        for k, v in zip(self._fields, values):
            setattr(self, k, v)

    def values(self, extra=True):
        last = None if extra else -self._extra_fields
        for field in self._fields[:last]:
            yield getattr(self, field)

    def update(self, new=None):
        if isinstance(self.prev, Mocky):
            self.prev.update(new=self.values())
        elif self._keep > 0:
            self.prev = self.__class__(
                *self._fields[:-self._extra_fields],
                extra_fields=self._fields[-self._extra_fields:],
                values=self.values(),
                keep=self._keep - 1)
            self.prev.future = self
        if isinstance(new, Mocky):
            new = new.values(extra=False)
        self._write(new)
        self.on_update()

    def has_props(self, **properties):
        for prop, val in properties.items():
            if prop not in self.__dict__:
                return False
            if self.__getattribute__(prop) != val:
                return False
        else:
            return True

    def has_dif_props(self, **properties):
        for prop, val in properties.items():
            if prop not in self.__dict__:
                return False
            if self.__getattribute__(prop) == val:
                return False
        else:
            return True

    def evals(self, *conditions):
        _locals = locals().copy()
        _locals.update(self.__dict__)
        return eval(' and '.join(conditions), globals(), _locals)

    def clear(self):
        pass

    def on_update(self):
        pass

    def dist(self, x, y=None):
        if isinstance(x, Mocky):
            x, y = x.x, x.y
        elif isinstance(x, list):
            x, y = x[0:2]
        return math.sqrt((self.x-x)**2 + (self.y-y)**2)

    def __str__(self):
        value_pairs = []
        for field in self._fields:
            value_pairs.append(f'{field}={self.__getattribute__(field)}')
        return ', '.join(value_pairs)


class MockyIter:
    def __init__(self, *props, keep=0, new_from=Mocky, parent=None,
                 props_filter=None, props_dif_filter=None, eval_filter=None):
        self.props = props
        self.keep = keep
        self.new_from = new_from
        self.props_filter = {} if props_filter is None else props_filter
        self.props_dif_filter = {} if props_dif_filter is None else props_dif_filter
        self.eval_filter = ['True'] if eval_filter is None else eval_filter
        self.items = [] if parent is None else parent.items

    def read(self, n):
        if n == len(self.items):
            for i in range(n):
                self.items[i].update()
        else:
            self.items.clear()
            for _ in range(n):
                item = self.new_from(*self.props, keep=self.keep)
                self.items.append(item)

    def _derive(self, **kwargs):
        filters = {
          'props_filter': kwargs.get('props_filter', self.props_filter),
          'props_dif_filter': kwargs.get('props_dif_filter', self.props_dif_filter),
          'eval_filter': kwargs.get('eval_filter', self.eval_filter),
        }
        return self.__class__(**filters, parent=self)

    def where(self, **props_filter):
        new_props_filter = self.props_filter.copy()
        new_props_filter.update(props_filter)
        return self._derive(props_filter=new_props_filter)

    def wherenot(self, **props_dif_filter):
        new_props_dif_filter = self.props_dif_filter.copy()
        new_props_dif_filter.update(props_dif_filter)
        return self._derive(props_dif_filter=new_props_dif_filter)

    def eval(self, eval_str):
        new_eval_filter = self.eval_filter.copy()
        new_eval_filter.append(eval_str)
        return self._derive(eval_filter=new_eval_filter)

    def filters(self, item):
        return (
            item.has_props(**self.props_filter)
            and item.has_dif_props(**self.props_dif_filter)
            and item.evals(*self.eval_filter)
        )

    def __iter__(self):
        for item in self.items:
            if self.filters(item):
                yield item

    def __len__(self):
        length = 0
        for item in self.items:
            if self.filters(item):
                length += 1
        return length

    def __str__(self):
        items_strings = []
        for item in self:
            items_strings.append(f'<{item}>')
        return '\n'.join(items_strings)


class MockyDictIter(MockyIter):
    def __init__(self, *props, **kwargs):
        super().__init__(*props, **kwargs)
        if 'parent' not in kwargs:
            self.items = {}

    def read(self, n):
        if not self.items:
            for _ in range(n):
                item = self.new_from(*self.props, keep=self.keep)
                self.items[item.id] = item
                item.on_update()
        else:
            non_updated_ids = list(self.items.keys())
            for _ in range(n):
                item = self.new_from(*self.props, keep=self.keep)
                if item.id in self.items:
                    self.items[item.id].update(new=item)
                    non_updated_ids.remove(item.id)
                else:
                    self.items[item.id] = item
            for item_id in non_updated_ids:
                self.items[item_id].clear()
                del self.items[item_id]

    def __getitem__(self, id_value):
        if id_value not in self.items:
            return None
        return self.items[id_value]

    def __iter__(self):
        for item in self.items.values():
            if self.filters(item):
                yield item

    def __len__(self):
        length = 0
        for item in self.items.values():
            if self.filters(item):
                length += 1
        return length


class MockyCell(Mocky):
    def __init__(self, *props, **kwargs):
        self.x = kwargs.pop('x', None)
        self.y = kwargs.pop('y', None)
        extra_fields = kwargs.setdefault('extra_fields', [])
        if 'x' not in extra_fields:
            extra_fields.append('x')
            extra_fields.append('y')
        self.row = kwargs.pop('row', None)
        super().__init__(*props, **kwargs)

    def _read_input(self):
        n = len(self._fields) - self._extra_fields
        return (int_or_raw(self.row[i]) for i in range(2 * self.x, n + 2 * self.x))


class Mocky2DIter(MockyIter):
    CLOSER = 0
    FASTER = 1

    def __init__(self, *props, **kwargs):
        if 'parent' in kwargs:
            self.width = kwargs['parent'].width
            self.height = kwargs['parent'].height
            self.slice = kwargs['parent'].slice
        else:
            self.width = kwargs.pop('width')
            self.height = kwargs.pop('height')
            self.slice = None
        kwargs.setdefault('new_from', MockyCell)
        super().__init__(*props, **kwargs)
        if not self.items:
            for x in range(self.width):
                self.items.append([])
                for y in range(self.height):
                    # Initializing empty cells
                    self.items[x].append(
                        self.new_from(
                            *self.props,
                            keep=self.keep,
                            values=[None]*len(self.props),
                            x=x,
                            y=y,
                        )
                    )

    def read(self):
        # This function assumes 2D cell data is provided per row
        for y in range(self.height):
            row = input().split()
            for x in range(self.width):
                self.items[x][y].row = row
                self.items[x][y].update()

    def choose_faster(self, x, y, dist):
        first_inc_x = max(-dist, -x)
        last_inc_x = min(dist, self.width - x - 1)
        for inc_x in range(first_inc_x, last_inc_x + 1):
            first_inc_y = max(abs(inc_x) - dist, -y)
            last_inc_y = min(dist - abs(inc_x), self.height - y - 1)
            for inc_y in range(first_inc_y, last_inc_y + 1):
                yield x + inc_x, y + inc_y

    def choose_closer(self, x, y, dist):
        def in_range(xpos, ypos):
            return self.width > xpos >= 0 and self.height > ypos >= 0

        if dist >= 0 and in_range(x, y):
            yield x, y
        if dist == math.inf:
            dist = max(x, self.width - x) + max(y, self.height - y)
        for cur_dist in range(1, dist + 1):
            for turn in (1, -1):
                for x_dist in range(turn * cur_dist, -turn * cur_dist, -turn):
                    cur_x = x + x_dist
                    cur_y = y + turn * (cur_dist - abs(x_dist))
                    if in_range(cur_x, cur_y):
                        yield cur_x, cur_y

    def __getitem__(self, key):
        def coords(z):
            if isinstance(z, tuple) or isinstance(z, list):
                return z[:2]
            elif isinstance(z, Mocky):
                return z.x, z.y
            else:
                raise TypeError(f"Invalid key index {z}.")

        if isinstance(key, slice):
            start = coords(key.start)
            stop = math.inf if key.stop is None else key.stop
            if isinstance(stop, int) or stop == math.inf:
                # Stop is a distance
                step = self.CLOSER if key.step is None else key.step
                if step not in (self.CLOSER, self.FASTER):
                    raise TypeError("Invalid step for a distance stop.")
            else:
                raise TypeError("Invalid stop")
            new_iter = self._derive()
            new_iter.slice = slice(start, stop, step)
            return new_iter
        else:
            coord = coords(key)
            return self.items[coord[0]][coord[1]]

    def __iter__(self):
        if self.slice is None:
            for row in self.items:
                for item in row:
                    if self.filters(item):
                        yield item
        else:
            if self.slice.step == self.CLOSER:
                sliced_cells = self.choose_closer
            elif self.slice.step == self.FASTER:
                sliced_cells = self.choose_faster
            else:
                raise TypeError("Invalid step")

            for x, y in sliced_cells(*self.slice.start, self.slice.stop):
                if self.filters(self.items[x][y]):
                    yield self.items[x][y]

    def __len__(self):
        length = 0
        if self.slice is None:
            for row in self.items:
                for item in row:
                    if self.filters(item):
                        length += 1
        else:
            if self.slice.step == self.CLOSER:
                sliced_cells = self.choose_closer
            elif self.slice.step == self.FASTER:
                sliced_cells = self.choose_faster
            else:
                raise TypeError("Invalid step")

            for x, y in sliced_cells(*self.slice.start, self.slice.stop):
                if self.filters(self.items[x][y]):
                    length += 1

        return length
