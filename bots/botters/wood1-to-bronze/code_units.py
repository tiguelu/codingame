from .defs import *


class Units(MockyDictIter):
    def read(self, n):
        # unit_type: UNIT, HERO, TOWER, can also be GROOT from wood1
        # shield: useful in bronze
        # stun_duration: useful in bronze
        # count_down_1: all countDown and mana vars are useful starting in bronze
        # hero_type: DEADPOOL, VALKYRIE, DOCTOR_STRANGE, HULK, IRONMAN
        # is_visible: 0 if it isn't
        # items_owned: useful from wood1
        props = [
            'id', 'team', 'unit_type', 'x', 'y', 'attack_range', 'health',
            'max_health', 'shield', 'attack_damage', 'movement_speed',
            'stun_duration', 'gold_value', 'count_down_1', 'count_down_2',
            'count_down_3', 'mana', 'max_mana', 'mana_regeneration',
            'hero_type', 'is_visible', 'items_owned'
        ]
        self.items = {}
        for i in range(n):
            super().read(*props)

    def hero(self, hero=0, from_enemy=False):
        i = 0
        team = enemy_team if from_enemy else my_team
        for unit in self.where(team=team, unit_type='HERO'):
            if i == hero:
                return unit
            i += 1
        return None

    def tower(self, from_enemy=False):
        team = enemy_team if from_enemy else my_team
        for unit in self.where(unit_type='TOWER', team=team):
            return unit


units = Units()
