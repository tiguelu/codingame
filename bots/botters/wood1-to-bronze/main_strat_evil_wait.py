from .defs import *
from .code import *


def strategy_evil_wait():
    hero = units.hero()
    enemy = units.hero(from_enemy=True)
    my_tower = units.tower()
    target_x = my_tower.x - side*95
    max_dist_to_tower = 200
    if hero.dist(target_x, hero.y) > max_dist_to_tower:
        print(f"MOVE {target_x} {hero.y}")
        return
    if hero.dist(target_x, hero.y) > 30:  # Hero occupies 30 of width
        for minion in units.where(unit_type='UNIT', team=my_team):
            if minion.x >= hero.x:
                break
        else:
            print(f"MOVE {target_x} {hero.y}")
            return

    if gold:
        for item in items.where(is_potion=1).wherenot(health=0):
            if item.cost <= gold and hero.health <= (hero.max_health - item.health):
                print(f"BUY {item.name}")
                return
        damage_candidate = [None, 0]
        for item in items.where(is_potion=0).wherenot(damage=0):
            if item.cost <= gold and hero.items_owned < 4:
                if item.damage > damage_candidate[1]:
                    damage_candidate = [item.name, item.damage]
        if damage_candidate[0] is not None:
            print(f"BUY {item.name}")
            return

    for minion in units.where(unit_type='UNIT', team=enemy_team):
        if minion.dist(hero) <= hero.attack_range + hero.movement_speed:
            print(f"ATTACK_NEAREST UNIT")
            return
    else:
        if enemy and hero.dist(enemy) <= hero.attack_range + hero.movement_speed:
            debug('dist enemy', enemy.id, hero.dist(enemy))
            print(f"ATTACK_NEAREST HERO")
            return

    print("WAIT")
