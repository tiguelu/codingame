from .defs import *

my_team = int(input())
enemy_team = int(not my_team)

# useful from wood1, represents the number of bushes and the number of places
# where neutral units can spawn
bush_and_spawn_point_count = int(input())
bushes_spawns = MockyIter()
for i in range(bush_and_spawn_point_count):
    # entity_type: BUSH, from wood1 it can also be SPAWN
    bushes_spawns.read('type', 'x', 'y', 'radius')
# debug(bushes_spawns)

item_count = int(input())  # useful from wood2
items = MockyIter()
item_fields = [
    'name', 'cost', 'damage', 'health', 'max_health', 'mana', 'max_mana',
    'move_speed', 'mana_regeneration', 'is_potion'
]
for i in range(item_count):
    # name: contains keywords such as BRONZE, SILVER and BLADE, BOOTS
    #       connected by "_" to help you sort easier
    # cost: BRONZE items have lowest cost and most expensive items are LEGENDARY
    # damage: keyword BLADE is present if the most important item stat is damage
    # move_speed: keyword BOOTS is present if the most important item stat
    #             is moveSpeed
    # is_potion: 0 if it's not instantly consumed
    items.read(*item_fields)
debug(items)
