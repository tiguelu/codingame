from .defs import *
from .code import *
from .main_strat_advancing import *
from .main_strat_evil_wait import *


while True:
    read_turn()
    if round_type == -2:
        print("IRONMAN")
        continue
    elif round_type == -1:
        print("VALKYRIE")
        continue

    strategy_advancing(hero=0)
    strategy_advancing(hero=1)
    # strategy_evil_wait()
