import sys
import math


DEBUG = True


def debug(*s, out=DEBUG):
    if out:
        print(*s, file=sys.stderr)


class Mocky:
    def __init__(self, *fields):
        self._fields = fields
        values = [int(value) if value.isnumeric() else value
                  for value in input().split()]
        for k, v in zip(fields, values):
            setattr(self, k, v)

    def has_props(self, **properties):
        for prop, val in properties.items():
            if prop not in self.__dict__:
                return False
            if self.__getattribute__(prop) != val:
                return False
        else:
            return True

    def has_dif_props(self, **properties):
        for prop, val in properties.items():
            if prop not in self.__dict__:
                return False
            if self.__getattribute__(prop) == val:
                return False
        else:
            return True

    def dist(self, x, y=None):
        if type(x) == Mocky:
            x, y = x.x, x.y
        return math.sqrt((self.x-x)**2 + (self.y-y)**2)

    def __str__(self):
        value_pairs = []
        for field in self._fields:
            value_pairs.append(f'{field}={self.__dict__[field]}')
        return ', '.join(value_pairs)


class MockyIter:
    def __init__(self, props_filter=None, props_dif_filter=None, items=None):
        self.props_filter = {} if props_filter is None else props_filter
        self.props_dif_filter = {} if props_dif_filter is None else props_dif_filter
        self.items = [] if items is None else items

    def read(self, *properties):
        item = Mocky(*properties)
        self.items.append(item)
        return item

    def where(self, **props_filter):
        new_props_filter = self.props_filter.copy()
        new_props_filter.update(props_filter)
        return MockyIter(props_filter=new_props_filter,
                         props_dif_filter=self.props_dif_filter, items=self.items)

    def wherenot(self, **props_dif_filter):
        new_props_filter = self.props_dif_filter.copy()
        new_props_filter.update(props_dif_filter)
        return MockyIter(props_filter=self.props_filter,
                         props_dif_filter=new_props_filter, items=self.items)

    def __iter__(self):
        items = self.items.values() if type(self.items) == dict else self.items
        for item in items:
            if item.has_props(**self.props_filter) and item.has_dif_props(**self.props_dif_filter):
                yield item

    def __str__(self):
        items_strings = []
        for item in self:
            items_strings.append(f'<{item}>')
        return '\n'.join(items_strings)


class MockyDictIter(MockyIter):
    def __init__(self):
        super().__init__(items={})

    def read(self, *properties, id_key='id'):
        item = Mocky(*properties)
        self.items[getattr(item, id_key)] = item
        return item

    def __getitem__(self, id_value):
        if id_value not in self.items:
            return None
        return self.items[id_value]
