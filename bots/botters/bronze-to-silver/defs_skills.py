SKILL_1 = 0
SKILL_2 = 1
SKILL_3 = 2
SKILL_COUNTDOWN = {SKILL_1: 'count_down_1',
                   SKILL_2: "count_down_2",
                   SKILL_3: 'count_down_3'}
IRONMAN = 'IRONMAN'
DEADPOOL = 'DEADPOOL'
HULK = 'HULK'
VALKYRIE = 'VALKYRIE'
DR_STRANGE = 'DOCTOR_STRANGE'

SKILLS = {
    DEADPOOL: {
        SKILL_1: {
            'name': 'COUNTER',
            'cost': 40,
            'range': 350,
        },
        SKILL_2: {
            'name': 'WIRE',
            'cost': 50,
            'range': 200,
        },
        SKILL_3 : {
            'name': 'STEALTH',
            'cost': 30,
            'range': None,
        }
    },
    IRONMAN: {
        SKILL_1: {
            'name': 'BLINK',
            'cost': 16,
            'range': 2000,
        },
        SKILL_2: {
            'name': 'FIREBALL',
            'cost': 60,
            'range': 900,
        },
        SKILL_3 : {
            'name': 'BURNING',
            'cost': 50,
            'range': 250,
        }
    },
    VALKYRIE: {
        SKILL_1: {
            'name': 'SPEARFLIP',
            'cost': 20,
            'range': 155,
        },
        SKILL_2: {
            'name': 'JUMP',
            'cost': 35,
            'range': 250,
        },
        SKILL_3 : {
            'name': 'POWERUP',
            'cost': 50,
            'range': None,
        }
    },
}


def skill_availability(hero, skill_idx):
    countdown = getattr(hero, SKILL_COUNTDOWN[skill_idx])
    if countdown > 0:
        return False
    if hero.mana < SKILLS[hero.hero_type][skill_idx]['cost']:
        return False
    return True
