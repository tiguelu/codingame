from .defs import *
from .code_units import *


def read_turn():
    global gold
    global enemy_gold
    global round_type
    global side

    gold = int(input())
    enemy_gold = int(input())
    # debug('my gold: ', gold, ', enemy: ', enemy_gold)
    round_type = int(input())  # if it's >0, it's number of heroes to control
    entity_count = int(input())
    units.read(entity_count)
    # debug(units)

    # side = LEFT(1) or RIGHT(-1), to use in formulas
    side = RIGHT if units.tower().x > WIDTH//2 else LEFT
