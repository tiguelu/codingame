from .defs import *
from .code import *


def strategy_advancing():
    hero = units.hero()
    tower = units.tower()
    enemy = units.hero(from_enemy=True)
    enemy_tower = units.tower(from_enemy=True)

    if hero.health <= hero.max_health//2:
        if hero.dist(tower) <= 50:
            for enemy in units.where(team=enemy_team, unit_type='HERO'):
                if hero.dist(enemy) <= hero.attack_range:
                    print(f"ATTACK {enemy.id}")
                    return
        else:
            print(f"MOVE {tower.x - side*50} {tower.y};Ouch!")
            return
    if hero.dist(enemy_tower) <= enemy_tower.attack_range+50:
        print(f"MOVE {tower.x} {tower.y}")
        return

    max_target_dist = 0
    for minion in units.where(unit_type='UNIT', team=my_team):
        if side == LEFT and minion.x >= hero.x:
            max_target_dist = max(max_target_dist, minion.dist(tower))
        elif side == RIGHT and minion.x <= hero.x:
            max_target_dist = max(max_target_dist, minion.dist(tower))
    if max_target_dist == 0:
        # There are no friendly units shielding our hero
        if hero.dist(tower) > 30:
            print(f"MOVE {tower.x - side*50} {tower.y}")
            return
    else:
        max_target_dist = int(max_target_dist)
        if hero.dist(tower.x + side*max_target_dist, tower.y) > hero.attack_range:
            print(f"MOVE {tower.x + side*(max_target_dist - hero.attack_range//2)} {tower.y}")
            return

    if gold:
        for item in items.where(is_potion=1).wherenot(health=0):
            if item.cost <= gold and hero.health <= (hero.max_health - item.health):
                print(f"BUY {item.name}")
                return
        damage_candidate = [None, 0]
        for item in items.where(is_potion=0).wherenot(damage=0):
            if item.cost <= gold and hero.items_owned < 3:
                if item.damage > damage_candidate[1]:
                    damage_candidate = [item.name, item.damage]
        if damage_candidate[0] is not None:
            print(f"BUY {damage_candidate[0]}")
            return

    for minion in units.where(unit_type='UNIT', team=enemy_team):
        if minion.dist(hero) <= hero.attack_range + 300:
            print(f"ATTACK_NEAREST UNIT")
            return

    if (
            enemy
            and hero.dist(enemy) <= hero.attack_range + hero.movement_speed
            and hero.dist(enemy_tower) - hero.movement_speed < enemy_tower.attack_range
    ):
        debug('dist enemy', enemy.id, hero.dist(enemy))
        print(f"ATTACK_NEAREST HERO")
        return

    print("WAIT")
