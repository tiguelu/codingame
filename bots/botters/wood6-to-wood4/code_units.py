from .defs import *


class Units:
    def __init__(self):
        self.units = {}

    def read_unit(self):
        # unit_type: UNIT, HERO, TOWER, can also be GROOT from wood1
        # shield: useful in bronze
        # stun_duration: useful in bronze
        # count_down_1: all countDown and mana vars are useful starting in bronze
        # hero_type: DEADPOOL, VALKYRIE, DOCTOR_STRANGE, HULK, IRONMAN
        # is_visible: 0 if it isn't
        # items_owned: useful from wood1
        fields = ['unit_id', 'team', 'unit_type', 'x', 'y', 'attack_range', 'health',
                  'max_health', 'shield', 'attack_damage', 'movement_speed',
                  'stun_duration', 'gold_value', 'count_down_1', 'count_down_2',
                  'count_down_3', 'mana', 'max_mana', 'mana_regeneration',
                  'hero_type', 'is_visible', 'items_owned']
        values = [int(value) if value.isnumeric() else value
                  for value in input().split()]
        unit = mocky_obj(fields, values)
        self.units[unit.unit_id] = unit
