import sys
import math

from unittest import mock

DEBUG = True


def debug(*s, out=DEBUG):
    if out:
        print(*s, file=sys.stderr)


def mocky_obj(fields, values):
    m = mock.Mock()
    m.configure_mock(**dict(zip(fields, values)))
    return m
