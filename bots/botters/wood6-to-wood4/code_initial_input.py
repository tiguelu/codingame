from .defs import *

my_team = int(input())

# useful from wood1, represents the number of bushes and the number of places
# where neutral units can spawn
bush_and_spawn_point_count = int(input())
bushes_spawns = []
for i in range(bush_and_spawn_point_count):
    # entity_type: BUSH, from wood1 it can also be SPAWN
    entity_type, x, y, radius = input().split()
    x = int(x)
    y = int(y)
    radius = int(radius)
    bushes_spawns.append([entity_type, x, y, radius])

item_count = int(input())  # useful from wood2
for i in range(item_count):
    # name: contains keywords such as BRONZE, SILVER and BLADE, BOOTS
    #       connected by "_" to help you sort easier
    # cost: BRONZE items have lowest cost and most expensive items are LEGENDARY
    # damage: keyword BLADE is present if the most important item stat is damage
    # move_speed: keyword BOOTS is present if the most important item stat
    #             is moveSpeed
    # is_potion: 0 if it's not instantly consumed
    fields = ['name', 'cost', 'damage', 'health', 'max_health', 'mana',
              'max_mana', 'move_speed', 'mana_regeneration', 'is_potion']
    values = [int(value) if value.isnumeric() else value
              for value in input().split()]
    item = mocky_obj(fields, values)
