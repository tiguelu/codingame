from .defs import *
from .code_units import *

units = Units()

while True:
    gold = int(input())
    enemy_gold = int(input())
    round_type = int(input())  # if it's >0, it's number of heroes to control
    entity_count = int(input())
    for i in range(entity_count):
        units.read_unit()

    # If roundType<0 we're choosing hero type
    # Else it is the number of heroes whose actions we need to output
    if round_type < 0:
        print("DEADPOOL")
    else:
        print("ATTACK_NEAREST TOWER")