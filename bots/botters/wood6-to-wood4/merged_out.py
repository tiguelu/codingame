############
# Content of defs.py
############
import sys
import math

from unittest import mock

DEBUG = True


def debug(*s, out=DEBUG):
    if out:
        print(*s, file=sys.stderr)


def mocky_obj(fields, values):
    m = mock.Mock()
    m.configure_mock(**dict(zip(fields, values)))
    return m


############
# Content of code_initial_input.py
############

my_team = int(input())

# useful from wood1, represents the number of bushes and the number of places
# where neutral units can spawn
bush_and_spawn_point_count = int(input())
bushes_spawns = []
for i in range(bush_and_spawn_point_count):
    # entity_type: BUSH, from wood1 it can also be SPAWN
    entity_type, x, y, radius = input().split()
    x = int(x)
    y = int(y)
    radius = int(radius)
    bushes_spawns.append([entity_type, x, y, radius])

item_count = int(input())  # useful from wood2
for i in range(item_count):
    # name: contains keywords such as BRONZE, SILVER and BLADE, BOOTS
    #       connected by "_" to help you sort easier
    # cost: BRONZE items have lowest cost and most expensive items are LEGENDARY
    # damage: keyword BLADE is present if the most important item stat is damage
    # move_speed: keyword BOOTS is present if the most important item stat
    #             is moveSpeed
    # is_potion: 0 if it's not instantly consumed
    fields = ['name', 'cost', 'damage', 'health', 'max_health', 'mana',
              'max_mana', 'move_speed', 'mana_regeneration', 'is_potion']
    values = [int(value) if value.isnumeric() else value
              for value in input().split()]
    item = mocky_obj(fields, values)


############
# Content of code_units.py
############


class Units:
    def __init__(self):
        self.units = {}

    def read_unit(self):
        # unit_type: UNIT, HERO, TOWER, can also be GROOT from wood1
        # shield: useful in bronze
        # stun_duration: useful in bronze
        # count_down_1: all countDown and mana vars are useful starting in bronze
        # hero_type: DEADPOOL, VALKYRIE, DOCTOR_STRANGE, HULK, IRONMAN
        # is_visible: 0 if it isn't
        # items_owned: useful from wood1
        fields = ['unit_id', 'team', 'unit_type', 'x', 'y', 'attack_range', 'health',
                  'max_health', 'shield', 'attack_damage', 'movement_speed',
                  'stun_duration', 'gold_value', 'count_down_1', 'count_down_2',
                  'count_down_3', 'mana', 'max_mana', 'mana_regeneration',
                  'hero_type', 'is_visible', 'items_owned']
        values = [int(value) if value.isnumeric() else value
                  for value in input().split()]
        unit = mocky_obj(fields, values)
        self.units[unit.unit_id] = unit


############
# Content of main.py
############

units = Units()

while True:
    gold = int(input())
    enemy_gold = int(input())
    round_type = int(input())  # if it's >0, it's number of heroes to control
    entity_count = int(input())
    for i in range(entity_count):
        units.read_unit()

    # If roundType<0 we're choosing hero type
    # Else it is the number of heroes whose actions we need to output
    if round_type < 0:
        print("DEADPOOL")
    else:
        print("ATTACK_NEAREST TOWER")


