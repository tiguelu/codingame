from .defs import *
from .code import *


def worth_skill_dr_strange(hero):
    # Using skill 2, Shield, if other hero being attacked
    if skill_availability(hero, SKILL_2):
        for friend in units.where(unit_type='HERO', team=my_team).wherenot(id=hero.id):
            if units.was_attacked(friend) and friend.health < friend.max_health//2:
                return f'SHIELD {friend.id}'

    # Using skill 3, Pull, against enemy?
    if skill_availability(hero, SKILL_3):
        if hero.mana >= 50:
            for enemy in units.where(unit_type='HERO', team=enemy_team):
                if hero.dist(enemy) <= 400:
                    return f'PULL {enemy.id}'

    return None
