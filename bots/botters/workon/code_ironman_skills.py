from .defs import *
from .code import *


def closer_to_enemy(unit):
    # Heroes can be hidden
    h1 = units.hero(FIRST_HERO)
    h2 = units.hero(SECOND_HERO)
    d1 = unit.dist(h1) if h1 else WIDTH
    d2 = unit.dist(h2) if h2 else WIDTH
    my_dist = min(d1, d2)
    h1 = units.hero(FIRST_ENEMY_HERO)
    h2 = units.hero(SECOND_ENEMY_HERO)
    d1 = unit.dist(h1) if h1 else WIDTH
    d2 = unit.dist(h2) if h2 else WIDTH
    enemy_dist = min(d1, d2)
    return enemy_dist < my_dist


def worth_skill_ironman(hero):
    # Using skill 2, Fireball, against enemy?
    if skill_availability(hero, SKILL_2):
        if hero.mana > 0.7 * hero.max_mana:
            # Mana is high so damage by fireball will be higher
            for enemy in units.where(unit_type='HERO', team=enemy_team):
                # Fireball higher with distance, checking if range is worth it
                if 800 < hero.dist(enemy) <= 900:
                    return f'FIREBALL {enemy.x} {enemy.y}'

    # Use skill 3, Burning, against either a groot or enemy?
    if skill_availability(hero, SKILL_3):
        # Burning is ideal if a groot is in range and closer to an enemy hero
        for groot in units.where(unit_type='GROOT'):
            if hero.dist(groot) <= 250 and closer_to_enemy(groot):
                # Groot in range and closer to an enemy hero
                return f"BURNING {groot.x} {groot.y}"
        # No groot was appropriate for the Bruning, checking enemies
        if hero.mana > 0.4 * hero.max_mana:
            for enemy in units.where(unit_type='HERO', team=enemy_team):
                if hero.dist(enemy) <= 250:
                    return f"BURNING {enemy.x} {enemy.y}"

    # Using skill 2, Fireball, against groot?
    if skill_availability(hero, SKILL_2):
        # This makes sense if mana is small but a groot is closer to an enemy
        if hero.mana < 0.3 * hero.max_mana:
            for groot in units.where(unit_type='GROOT'):
                # Is groot in range and closer to an enemy?
                if hero.dist(groot) <= 900 and closer_to_enemy(groot):
                    return f'FIREBALL {groot.x} {groot.y}'

    return None
