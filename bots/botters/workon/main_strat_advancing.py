from .defs import *
from .code import *


def strategy_advancing(hero_idx):
    global gold

    hero = units.hero(hero_idx)
    if not hero:
        # Oh no, this hero was killed
        return
    tower = units.tower()
    enemy_tower = units.tower(from_enemy=True)

    if hero.health <= hero.max_health//4:
        # Hero is being attacked and lost health
        if hero.dist(tower) <= 50:
            # Hero is near the tower
            for enemy in units.where(team=enemy_team, unit_type='HERO'):
                if hero.dist(enemy) <= hero.attack_range:
                    # An enermy hero is within range, possibly attacking us
                    print(f"ATTACK {enemy.id}")
                    return
        else:
            # Hero is not near the tower, but being injured, she must get back
            print(f"MOVE {tower.x - side*50} {tower.y};Charlie bit me!")
            return

    if hero.dist(enemy_tower) <= enemy_tower.attack_range+50:
        # We're too close to the enemy tower. Moving back
        print(f"MOVE {tower.x} {tower.y};Bye bye!")
        return

    # Checking if hero can be attacked by groot
    for groot in units.where(unit_type='GROOT'):
        if hero.dist(groot) <= hero.attack_range:
            print(f'ATTACK_NEAREST GROOT')
            return

    # Checking if the hero is shielded by her minions, otherwise retreat!
    max_target_dist = 0  # Max distance between tower and a friendly minion
    for minion in units.where(unit_type='UNIT', team=my_team):
        minion_is_shielding = (
                (side == LEFT and minion.x >= hero.x)
                or
                (side == RIGHT and minion.x <= hero.x)
        )
        if minion_is_shielding:
            max_target_dist = max(max_target_dist, minion.dist(tower))
    if max_target_dist == 0:
        # There are no friendly minions shielding our hero
        if hero.dist(tower) > 30:
            # If hero is away from the tower, she retreats to shelter
            print(f"MOVE {tower.x - side*50} {tower.y};Chicken retreat!")
            return

    # There is at least one minion shielding the hero
    max_target_dist = int(max_target_dist)

    # Checking first if it's worth using a skill
    skill_command = None
    if hero.hero_type == IRONMAN:
        skill_command = worth_skill_ironman(hero)
    elif hero.hero_type == DR_STRANGE:
        skill_command = worth_skill_dr_strange(hero)
    if skill_command is not None:
        print(skill_command)
        return

    # If the distance between the hero and the furthest minion is bigger than
    # the hero's attack range, she can still move closer to the minion
    if hero.dist(tower.x + side*max_target_dist, tower.y) > hero.attack_range:
        print(f"MOVE {tower.x + side*(max_target_dist - hero.attack_range)} {tower.y}")
        return

    # At this point we know we don't have to retreat, move to a better position,
    # defend from an attacking hero or any critical action

    if gold:
        # If we're rich, check first if healing is needed
        for item in items.where(is_potion=1).wherenot(health=0):
            if item.cost <= gold and hero.health <= (hero.max_health - item.health):
                print(f"BUY {item.name}")
                gold -= item.cost
                return
        # If healing is not needed, or affordable, check if we can increase damage
        if hero.items_owned < 3:
            damage_candidate = [None, 0]
            for item in items.where(is_potion=0).wherenot(damage=0):
                if item.cost <= gold:
                    if item.damage > damage_candidate[1]:
                        damage_candidate = [item, item.damage]
            if damage_candidate[0] is not None:
                print(f"BUY {damage_candidate[0].name}")
                gold -= damage_candidate[0].cost
                return

    # Check if any minion is attackable
    for minion in units.where(unit_type='UNIT', team=enemy_team):
        if hero.dist(minion) <= hero.attack_range + hero.movement_speed + 100:
            # Minion is within range or not too far
            print(f"ATTACK_NEAREST UNIT")
            return

    # Check if any enemy is attackable
    for enemy in units.where(unit_type='HERO', team=enemy_team):
        if (
                hero.dist(enemy) <= hero.attack_range + hero.movement_speed
                and hero.dist(enemy_tower) - hero.movement_speed < enemy_tower.attack_range
        ):
            debug('dist enemy', enemy.id, hero.dist(enemy))
            print(f"ATTACK_NEAREST HERO")
            return

    # Lazy daisy
    print("WAIT")
