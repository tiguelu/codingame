from .defs import *
from .code import *
from .main import *


while True:
    read_turn()
    if round_type == -2:
        print("IRONMAN")
        continue
    elif round_type == -1:
        print("DOCTOR_STRANGE")
        continue

    strategy_advancing(FIRST_HERO)
    strategy_advancing(SECOND_HERO)
