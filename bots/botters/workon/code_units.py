from .defs import *


class Units(MockyDictIter):
    def __init__(self):
        super().__init__()
        self.prev_data = {}
        self.heroes_ids = {
            FIRST_HERO: None, SECOND_HERO: None,
            FIRST_ENEMY_HERO: None, SECOND_ENEMY_HERO: None,
        }

    def read(self, n):
        # unit_type: UNIT, HERO, TOWER, can also be GROOT from wood1
        # shield: useful in bronze
        # stun_duration: useful in bronze
        # count_down_1: all countDown and mana vars are useful starting in bronze
        # hero_type: DEADPOOL, VALKYRIE, DOCTOR_STRANGE, HULK, IRONMAN
        # is_visible: 0 if it isn't
        # items_owned: useful from wood1
        props = [
            'id', 'team', 'unit_type', 'x', 'y', 'attack_range', 'health',
            'max_health', 'shield', 'attack_damage', 'movement_speed',
            'stun_duration', 'gold_value', SKILL_COUNTDOWN[SKILL_1],
            SKILL_COUNTDOWN[SKILL_2], SKILL_COUNTDOWN[SKILL_3], 'mana', 'max_mana',
            'mana_regeneration', 'hero_type', 'is_visible', 'items_owned'
        ]
        for hero in [FIRST_HERO, SECOND_HERO]:
            hero_id = self.heroes_ids[hero]
            if hero_id in self.items:
                self.prev_data[hero_id] = self.items[hero_id]
        self.items = {}
        for i in range(n):
            unit = super().read(*props)
            if unit.unit_type == 'HERO':
                debug(unit)
                if unit.id not in self.heroes_ids.values():
                    if unit.team == my_team:
                        if self.heroes_ids[FIRST_HERO] is None:
                            self.heroes_ids[FIRST_HERO] = unit.id
                        else:
                            self.heroes_ids[SECOND_HERO] = unit.id
                    else:
                        if self.heroes_ids[FIRST_ENEMY_HERO] is None:
                            self.heroes_ids[FIRST_ENEMY_HERO] = unit.id
                        else:
                            self.heroes_ids[SECOND_ENEMY_HERO] = unit.id

    def dif(self, hero_id, attribute):
        if hero_id not in self.prev_data:
            return None
        hero = self.items[hero_id]
        prev = self.prev_data[hero_id]
        return getattr(hero, attribute) - getattr(prev, attribute)

    def was_attacked(self, hero):
        change = self.dif(hero.id, 'health')
        return change is not None or change != 0

    def hero(self, hero_idx):
        hero_not_found = (
                hero_idx not in self.heroes_ids
                or self.heroes_ids[hero_idx] not in self.items)
        if hero_not_found:
            return None
        return self.items[self.heroes_ids[hero_idx]]

    def tower(self, from_enemy=False):
        team = enemy_team if from_enemy else my_team
        for unit in self.where(unit_type='TOWER', team=team):
            return unit


units = Units()
